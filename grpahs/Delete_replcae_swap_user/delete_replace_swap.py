import json

import copy

with open(
        'data.json') as f:
    data = json.load(f)


# data.pop('E-1')
# data.pop('E-2')
# data.pop('E-3')
# data.pop('E-4')
# data.pop('E-5')
# data.pop('E-6')
# data.pop('E-7')
# data.pop('E-8')
# data.pop('E-10')
# data.pop('E-12')
# data.pop('E-9')


# ================================ Get Users vs device id ===============================
#
# users_device_id = {}
# for i, j in data.items():
#     users_device_id.update({i: j['device_id']})


# contact_events = data['E-5']['contact_event']

def delete_user(request, regions, user):
    # user = 'E-1'

    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )

    device_id = users_device_id[user]

    resp = {}
    for k in alluser:
        contact_events = json.loads(k.contact_event)
        contact_events_copy = copy.deepcopy(contact_events)
        for i in contact_events:
            remote = i['remote']
            if remote == device_id:
                contact_events_copy.remove(i)
        k.contact_event = json.dumps(contact_events_copy)
        k.save()
    return Response({'status': 'success'})


# len(data['E-3']['contact_event'])

def replace_device_id(request, regions, target_user, new_id):
    target_user = 'E-1'
    new_id = 'fun'

    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )

    old_id = users_device_id[target_user]

    for k in alluser():
        contact_events = json.loads(k.contact_event)
        duplicate_event = []
        if k.user == target_user:
            for eve in contact_events:
                eve['local'] = new_id
                duplicate_event.append(eve)

        else:
            for eve in contact_events:
                remote = eve['remote']
                if remote == old_id:
                    eve['remote'] = new_id
                    duplicate_event.append(eve)
                else:
                    duplicate_event.append(eve)

        k.contact_event = json.dumps(duplicate_event)
        k.save()

    return Response({'status': 'success'})


def swap_user_devices(request, regions, user_1, user_2):

    user_1 = 'E-1'
    user_2 = 'E-2'

    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})


    user_1_id = users_device_id[user_1]
    user_2_id = users_device_id[user_2]

    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )


    for k in alluser:
        contact_events = json.loads(k.contact_event)
        duplicate_event = []
        if k.user == user_1:
            for eve in contact_events:
                eve['local'] = user_2_id
                remote = eve['remote']
                if remote == user_2_id:
                    eve['remote'] = user_1_id
                duplicate_event.append(eve)

        elif k.user == user_2:
            for eve in contact_events:
                eve['local'] = user_1_id
                duplicate_event.append(eve)
                remote = eve['remote']
                if remote == user_1_id:
                    eve['remote'] = user_2_id
                duplicate_event.append(eve)

        else:
            for eve in contact_events:
                remote = eve['remote']
                if remote == user_1_id:
                    eve['remote'] = user_2_id
                elif remote == user_2_id:
                    eve['remote'] = user_1_id
                duplicate_event.append(eve)

        k.contact_event = json.dumps(duplicate_event)
        k.save()

    return Response({'status': 'success'})