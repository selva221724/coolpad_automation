import json

import copy

with open(
        'data_updated.json') as f:
    data = json.load(f)


# data.pop('E-1')
# data.pop('E-2')
# data.pop('E-3')
# data.pop('E-4')
# data.pop('E-5')
# data.pop('E-6')
# data.pop('E-7')
# data.pop('E-8')
# data.pop('E-10')
# data.pop('E-12')
# data.pop('E-9')


# ================================ Get Users vs device id ===============================

# users_device_id = {}
# for i, j in data.items():
#     users_device_id.update({i: j['device_id']})


# data,startTime, endTime, current_date = data,'2020-09-13','2020-09-18','2020-09-17'
# u = 'E-11'
# user = data['E-11']


def invalid_data_removal(request, contact_events, regions):
    data = default_getall(regions)
    # contact_events = data['E-5']['contact_event']

    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    valid_device_ids = list(users_device_id.values())

    contact_events_copy = copy.deepcopy(contact_events)
    for i in contact_events:
        remote = i['remote']
        if remote not in valid_device_ids:
            contact_events_copy.remove(i)

    return Response(contact_events_copyreturn)
