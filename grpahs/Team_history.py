import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


def team_history(data, startTime, endTime, current_date, users_device_id):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})

        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        # =================== removing the own users count =====================

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if e in temp_team_list:
                    result_dict[i].pop(e)

        # ======================== no of users in contact day ==================

        count_dict = {}
        for i, j in result_dict.items():
            for k, v in j.items():
                if not v == 0:
                    if k in count_dict:
                        count_dict[k] += v
                    else:
                        count_dict.update({k: v})

        no_of_users_in_contact = len(count_dict)

        # ========================= no of contacts in a day =============================
        no_of_contacts = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts += j

        # ========================== Users with maximum contact in a day ===================

        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})

        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))
        top_five_users_max_no_contact = dict(list(over_al_cont.items())[:5])

        # ============================ Most contact user pair in a day ======================

        most_contact_user_pair = max_device[0]
        primery_user = list(most_contact_user_pair.keys())[0]
        second_user = list(list(most_contact_user_pair.values())[0].keys())[0]
        second_user = [a for a, v in users_device_id.items() if v == second_user][0]
        contact_count = list(list(most_contact_user_pair.values())[0].values())[0]
        most_contact_user_pair_day = {primery_user + ' - ' + second_user: contact_count}

        # ============================================================================================
        # ================================== week wise ==============================================

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})

        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        # =================== removing the own users count =====================

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if e in temp_team_list:
                    result_dict[i].pop(e)

        # ========================= no of contacts in a week =============================
        no_of_contacts_week = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts_week += j

        # ========================== Users with maximum contact in a week ===================

        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})

        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))
        top_five_users_max_no_contact_week = dict(list(over_al_cont.items())[:5])

        # ============================ Most contact user pair in a week ======================

        most_contact_user_pair = max_device[0]
        primery_user = list(most_contact_user_pair.keys())[0]
        second_user = list(list(most_contact_user_pair.values())[0].keys())[0]
        second_user = [a for a, v in users_device_id.items() if v == second_user][0]
        contact_count = list(list(most_contact_user_pair.values())[0].values())[0]
        most_contact_user_pair_week = {primery_user + ' - ' + second_user: contact_count}

        # =============================== Final dict append =========================

        team_dict.update({t: {
            'Users in Contact': no_of_users_in_contact,
            'Contacts in a Day': no_of_contacts,
            'Contacts in a Week': no_of_contacts_week,
            'top 5 users in contact Day': top_five_users_max_no_contact,
            'top 5 users in contact Week': top_five_users_max_no_contact_week,
            'Most contact user pair Day': most_contact_user_pair_day,
            'Most contact user pair Week': most_contact_user_pair_week

        }})

    return team_dict


teamhistory = team_history(data, startTime, endTime, current_date, users_device_id)
