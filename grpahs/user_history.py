import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 24, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 27, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 27, 0, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


def User_history_data(data, startTime, endTime, current_date, users_device_id):
    user_data = {}
    for u, user in data.items():
        print(u)
        device_id = user['device_id']
        contact_events = user['contact_event']

        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})

        if not sliced_events == []:

            max_device = []
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: 0})
                for event in j:
                    temp[event['remote']] += 1
                pair_count.update({i: temp})

                max_key = max(temp, key=lambda k: temp[k])
                max_device.append({i: {max_key: temp[max_key]}})

        # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

        # ========================= no of contacts in single day =============================
        if not sliced_events == []:
            no_of_contacts = 0
            for i in pair_count.values():
                for j in i.values():
                    if not j == 0:
                        no_of_contacts += 1
        else:
            no_of_contacts = 0

        # ============================== max contact duration with user ===================
        if not sliced_events == []:
            max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]

            temp_max = {}
            for i in max_duration:
                temp_max.update({list(i.keys())[0]: list(i.values())[0]})

            max_user, max_duration = max(temp_max.items(), key=lambda k: k[1])
            max_user = [a for a, v in users_device_id.items() if v == max_user][0]

            max_duration_day = ms_to_time(max_duration) + '  ' + max_user
        else:
            max_duration_day = None

        # ============================= contact history single day  ==========================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: {'count': 0, 'max_duration': []}})
            for event in j:
                temp[event['remote']]['count'] += 1
                temp[event['remote']]['max_duration'].append(event['duration'])

            for t, tt in temp.items():
                try:
                    max_dur = max(tt['max_duration'])
                    temp[t]['max_duration'] = ms_to_time(max_dur)
                except:
                    temp[t]['max_duration'] = None

            pair_count.update({i: temp})

        contact_history = {}
        for i in pair_count.values():
            for j, k in i.items():
                if not k['count'] == 0:
                    second_emp = [a for a, v in users_device_id.items() if v == j][0]
                    contact_history.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})

        # =========================================================================================
        # ================================== week wise ============================================
        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})
        if not sliced_events == []:
            max_device = []
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: 0})
                for event in j:
                    temp[event['remote']] += 1
                pair_count.update({i: temp})

                max_key = max(temp, key=lambda k: temp[k])
                max_device.append({i: {max_key: temp[max_key]}})

        # ========================= no of contacts in week =============================
        if not sliced_events == []:
            no_of_contacts_week = 0
            for i in pair_count.values():
                for j in i.values():
                    if not j == 0:
                        no_of_contacts_week += 1
        else:
            no_of_contacts_week = 0

        # ============================== max contact duration with user week ===================
        if not sliced_events == []:

            max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]

            temp_max = {}
            for i in max_duration:
                temp_max.update({list(i.keys())[0]: list(i.values())[0]})

            max_user, max_duration = max(temp_max.items(), key=lambda k: k[1])
            max_user = [a for a, v in users_device_id.items() if v == max_user][0]

            # max_duration = max([[j['duration'] for j in i] for i in max_pair.values()][0])
            max_duration_week = ms_to_time(max_duration) + '  ' + max_user
        else:
            max_duration_week = None

        # ============================= contact history week  ==========================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: {'count': 0, 'max_duration': []}})
            for event in j:
                temp[event['remote']]['count'] += 1
                temp[event['remote']]['max_duration'].append(event['duration'])

            for t, tt in temp.items():
                try:
                    max_dur = max(tt['max_duration'])
                    temp[t]['max_duration'] = ms_to_time(max_dur)
                except:
                    temp[t]['max_duration'] = None

            pair_count.update({i: temp})

        contact_history_week = {}
        for i in pair_count.values():
            for j, k in i.items():
                if not k['count'] == 0:
                    second_emp = [a for a, v in users_device_id.items() if v == j][0]
                    contact_history_week.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
        # ========================================= clock chart =================================================

        hours_list = []
        hours_list.append(current_date)
        for i in range(24):
            mints_back = current_date + datetime.timedelta(hours=i + 1)
            hours_list.append(mints_back)

        hour_wise_count = []
        for hr in range(len(hours_list) - 1):

            mints_back = hours_list[hr]
            current_date = hours_list[hr + 1]
            # ================================ slice the all events by given date================
            all_events = []

            if not contact_events == []:
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                    if isNowInTimePeriod(mints_back, current_date, event_time):
                        all_events.append(event)

            result = len(all_events)
            hour_wise_count.append(result)
        # =============================== Final dict append =========================

        user_data.update({u: {
            'Current Device ID': device_id,
            'Users in Contact Day': no_of_contacts,
            'Users in Contact Week': no_of_contacts_week,
            'Max Contact Duration Day': max_duration_day,
            'Max Contact Duration Week': max_duration_week,
            'Contact History Day': contact_history,
            'Contact History Week': contact_history_week,
            '24_hr_clock': hour_wise_count

        }})

    return user_data


userdata = User_history_data(data, startTime, endTime, current_date, users_device_id)
