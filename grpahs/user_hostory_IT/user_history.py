import copy
import datetime
import json
import random
import sys
import dateutil.relativedelta
import pytz
from natsort import natsorted



import timeit

start = timeit.default_timer()

timezone = pytz.timezone('America/Chicago')

# with open(
#         'data.json') as f:
#     data = json.load(f)

import requests

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
r = requests.get(tyson)
data = r.json()


def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    distance = round(float(distance), 4)
    distance = str(distance) + ' ft'
    # print(distance,"thambi")
    return distance


def dateTOtimestamp(startdate):
    d, t = startdate.split(' ')
    d = d.split('-')
    t = t.split('-')
    event_time = datetime.datetime(int(d[0]), int(d[1]), int(d[2]),
                                   hour=int(t[0]), minute=int(t[1]), second=int(t[2]))

    startTime = timezone.localize(event_time)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + "h " + str(minutes) + 'm ' + str(seconds) + "s"
    return a


def time_to_ms(a):
    # a = '0:4:49'
    a = a.split(' ')
    b = [int(a[0][:-1]) * 60 * 60 * 1000, int(a[1][:-1]) * 60 * 1000, int(a[2][:-1]) * 1000]
    return int(sum(b))


#

# # #
# data, start_date, end_date, startTime, endTime, = data, '2020-10-11 00-00-00', '2020-10-18 00-00-00', \
#                                                   '2020-10-15 00-00-00', '2020-10-15 23-59-59'

data, start_date, end_date, startTime, endTime, = data, '2020-10-18 00-00-00', '2020-10-25 00-00-00', \
                                                  '2020-10-20 00-00-00', '2020-10-20 23-59-59'

# def User_history_data(request, start_date, end_date, startTime, endTime, regions):
# data = default_getall(regions)

sp = startTime.split(' ')[0]
sp = sp.split('-')
current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
current_date = timezone.localize(current_date)

start_date = dateTOtimestamp(start_date)
end_date = dateTOtimestamp(end_date)
startTime = dateTOtimestamp(startTime)
endTime = dateTOtimestamp(endTime)

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# =================================== day all =========================================

max_pair1 = {}
for i, j in data.items():

    sliced_events = []
    contact_events = j['contact_event']
    # if not contact_events == []:
    for event in contact_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if isNowInTimePeriod(startTime, endTime, event_time):
            sliced_events.append(event)
    max_pair1.update({i: sliced_events})

pair_count1 = {}
for i, j in max_pair1.items():
    temp = {}
    for users in users_device_id.values():
        emp_name = [a for a, v in users_device_id.items() if v == users][0]
        temp.update({emp_name: {'count': 0, 'max_duration': []}})
    for event in j:
        remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
        temp[remote]['count'] += 1
        temp[remote]['max_duration'].append(event['duration'])

    for t, tt in temp.items():
        try:
            max_dur = max(tt['max_duration'])
            temp[t]['max_duration'] = ms_to_time(max_dur)
        except:
            temp[t]['max_duration'] = None

    temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

    pair_count1.update({i: temp})

target_dict = copy.deepcopy(pair_count1)
f = copy.deepcopy(pair_count1)

for k in pair_count1.keys():
    for i, j in f[k].items():
        temp_user = i
        temp_user_count = j['count']
        if not i == k:
            user_count = pair_count1[i][k]['count']
            if user_count > temp_user_count:
                print('yes')
                target_dict[k][i]['count'] = user_count
                target_dict[k][i]['max_duration'] = pair_count1[i][k]['max_duration']

temp_target = copy.deepcopy(target_dict)
for i, j in temp_target.items():
    for k in j.keys():
        dev_id = users_device_id[k]
        target_dict[i][dev_id] = target_dict[i].pop(k)

# =================================== week all =========================================

max_pair2 = {}
for i, j in data.items():

    sliced_events = []
    contact_events = j['contact_event']
    # if not contact_events == []:
    for event in contact_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if isNowInTimePeriod(start_date, end_date, event_time):
            sliced_events.append(event)
    max_pair2.update({i: sliced_events})

pair_count2 = {}
for i, j in max_pair2.items():
    temp = {}
    for users in users_device_id.values():
        emp_name = [a for a, v in users_device_id.items() if v == users][0]
        temp.update({emp_name: {'count': 0, 'max_duration': []}})
    for event in j:
        remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
        temp[remote]['count'] += 1
        temp[remote]['max_duration'].append(event['duration'])

    for t, tt in temp.items():
        try:
            max_dur = max(tt['max_duration'])
            temp[t]['max_duration'] = ms_to_time(max_dur)
        except:
            temp[t]['max_duration'] = None

    temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

    pair_count2.update({i: temp})

target_dict2 = copy.deepcopy(pair_count2)
f2 = copy.deepcopy(pair_count2)

for k in pair_count2.keys():
    for i, j in f2[k].items():
        temp_user = i
        temp_user_count = j['count']
        if not i == k:
            user_count = pair_count2[i][k]['count']
            if user_count > temp_user_count:
                target_dict2[k][i]['count'] = user_count
                target_dict2[k][i]['max_duration'] = pair_count2[i][k]['max_duration']

temp_target2 = copy.deepcopy(target_dict2)
for i, j in temp_target2.items():
    for k in j.keys():
        dev_id = users_device_id[k]
        target_dict2[i][dev_id] = target_dict2[i].pop(k)
# ============================================================================

user_data = {}

for u, user in data.items():
    # count = 0
    # print(u)
    device_id = user['device_id']
    contact_events = user['contact_event']

    max_pair = {}
    sliced_events = []
    for event in contact_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if isNowInTimePeriod(startTime, endTime, event_time):
            sliced_events.append(event)
    max_pair.update({u: sliced_events})
    contact_all = copy.deepcopy(sliced_events)
    # if not sliced_events == []:
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ========================== merger pair count =========================================
    temp_pair = pair_count
    for k in temp_pair.values():
        for i, j in k.items():
            count_1 = j
            count_tar = target_dict[u][i]['count']
            if count_tar > count_1:
                pair_count[u][i] = count_tar

    # ========================= no of contacts in single day =============================
    no_of_contacts = 0
    for i in pair_count.values():
        for j in i.values():
            if not j == 0:
                no_of_contacts += 1
    # =============================== contact event all ===================================
    contact_event_all = []
    for i in contact_all:
        remote = i['remote']
        remote = [a for a, v in users_device_id.items() if v == remote][0]
        time_st = datetime.datetime.utcfromtimestamp(i['start'] / 1000)
        time_st = time_st.replace(tzinfo=pytz.utc).astimezone(timezone)
        time_st = time_st.strftime("%I:%M %p  %D")
        avgdist = int(i['avgDist']) / 10
        minDist = int(i['minDist']) / 10
        avgdist = cm_to_ft(avgdist)
        minDist = cm_to_ft(minDist)
        temp_dict = {
            remote: {'timestamp': time_st, 'duration': ms_to_time(i['duration']),
                     'avgDist': avgdist, 'minDist': minDist}
        }
        contact_event_all.append(temp_dict)
    # ============================== max contact duration with user ===================

    max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
    for i, j in target_dict[u].items():
        if not j['max_duration'] is None:
            max_duration.append({i: time_to_ms(j['max_duration'])})

    if not max_duration == []:
        max_dur = max(max_duration, key=lambda x: list(x.values())[0])
        max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
        max_user = [a for a, v in users_device_id.items() if v == max_user][0]
        max_duration_day = ms_to_time(max_duration) + '  ' + max_user
    else:
        max_duration_day = None

    # else:
    #     no_of_contacts = 0
    #     max_duration_day = None
    #     contact_event_all = []
    # ========================================= clock chart =================================================
    hours_list = [current_date]
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # mints_back = dateTOtimestamp(mints_back)
        # ================================ slice the all events by given date================
        all_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_date_1, event_time):
                if isNowInTimePeriod(startTime, endTime, event_time):
                    all_events.append(event)
            # count = count + 1
        result = len(all_events)
        hour_wise_count.append(result)
    # ============================= contact history single day  ==========================
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            # emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({users: {'count': 0, 'max_duration': []}})
        for event in j:
            temp[event['remote']]['count'] += 1
            temp[event['remote']]['max_duration'].append(event['duration'])
        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None
        pair_count.update({i: temp})

    # ========================== merger pair count =========================================
    temp_pair = pair_count
    for k in temp_pair.values():
        for i, j in k.items():
            count_1 = j['count']
            count_tar = target_dict[u][i]['count']
            if count_tar > count_1:
                pair_count[u][i]['count'] = count_tar
                pair_count[u][i]['max_duration'] = target_dict[u][i]['max_duration']

    contact_history = {}
    for i in pair_count.values():
        for j, k in i.items():
            if not k['count'] == 0:
                second_emp = [a for a, v in users_device_id.items() if v == j][0]
                contact_history.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
    #         # =========================================================================================
    #         # ================================== week wise ============================================
    max_pair = {}
    sliced_events = []
    for event in contact_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if isNowInTimePeriod(start_date, end_date, event_time):
            sliced_events.append(event)
    max_pair.update({u: sliced_events})

    # if not sliced_events == []:
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # ========================== merger pair count =========================================
    temp_pair = pair_count
    for k in temp_pair.values():
        for i, j in k.items():
            count_1 = j
            count_tar = target_dict2[u][i]['count']
            if count_tar > count_1:
                pair_count[u][i] = count_tar

    # ========================= no of contacts in week =============================
    no_of_contacts_week = 0
    for i in pair_count.values():
        for j in i.values():
            if not j == 0:
                no_of_contacts_week += 1
    # ============================== max contact duration with user week ===================

    max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
    for i, j in target_dict2[u].items():
        if not j['max_duration'] is None:
            max_duration.append({i: time_to_ms(j['max_duration'])})

    if not max_duration == []:
        max_dur = max(max_duration, key=lambda x: list(x.values())[0])
        max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
        max_user = [a for a, v in users_device_id.items() if v == max_user][0]
        max_duration_week = ms_to_time(max_duration) + '  ' + max_user
    else:
        max_duration_week = None

    # ============================= contact history week  ==========================
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: {'count': 0, 'max_duration': []}})
        for event in j:
            temp[event['remote']]['count'] += 1
            temp[event['remote']]['max_duration'].append(event['duration'])
        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None
        pair_count.update({i: temp})

    # ========================== merger pair count =========================================
    temp_pair = pair_count
    for k in temp_pair.values():
        for i, j in k.items():
            count_1 = j['count']
            count_tar = target_dict2[u][i]['count']
            if count_tar > count_1:
                pair_count[u][i]['count'] = count_tar
                pair_count[u][i]['max_duration'] = target_dict2[u][i]['max_duration']

    contact_history_week = {}
    for i in pair_count.values():
        for j, k in i.items():
            if not k['count'] == 0:
                second_emp = [a for a, v in users_device_id.items() if v == j][0]
                contact_history_week.update(
                    {second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
    # =============================== Final dict append =========================
    user_data.update({u: {
        'Current Device ID': device_id,
        'Device History': user['history_of_ids'],
        'Users in Contact Day': no_of_contacts,
        'Users in Contact Week': no_of_contacts_week,
        'Max Contact Duration Day': max_duration_day,
        'Max Contact Duration Week': max_duration_week,
        'Contact History Day': contact_history,
        'Contact History Week': contact_history_week,
        '24_hr_clock': hour_wise_count,
        'contact_event_all': contact_event_all
    }})
    # return Response(user_data)

# final = User_history_data(data, '2020-10-11', '2020-10-17', '2020-10-13')
#
# print(final['T-17']['Contact History Day'])

print(user_data['T-1'])


#==========================================================================
stop = timeit.default_timer()
total_time = stop - start

# output running time in a nice format.
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)

sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))

