import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz

timezone = pytz.timezone('America/Chicago')

with open(
        'data.json') as f:
    data = json.load(f)


def dateTOtimestamp(startdate):
    t = startdate.split('-')
    event_time = datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
    startTime = event_time.replace(tzinfo=timezone).astimezone(timezone)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


startTime = '2020-09-25'
endTime = '2020-09-28'



# def daily_tracker_get(request, current_date, regions):

# sp = start_date.split('-')
# startTime = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
# startTime = startTime.replace(tzinfo=timezone).astimezone(timezone)
#
#
# sp = end_date.split('-')
# endTime = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
# endTime = endTime.replace(tzinfo=timezone).astimezone(timezone)


# data = default_getall(regions)
users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})


team_list = [j['team'] for i, j in data.items()]
team_list = natsorted(list(set(team_list)))

data_team = {}
startTime = dateTOtimestamp(startTime)
endTime = dateTOtimestamp(endTime)
for team in team_list:
    temp_data = {}
    for i, j in data.items():
        temp_team = j['team']
        if temp_team == team:
            temp_data.update({i: j})
    data_team.update({team: temp_data})

team_dict = {}
for t, team in data_team.items():

    all_events = []
    max_pair = {}
    for i, j in team.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================

    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i, j in pair_count.items():
        temp = {}
        for emp, k in j.items():
            emp_id = [k for k, v in users_device_id.items() if v == emp][0]
            temp.update({emp_id: k})
        final_dict.update({i: temp})

    for i, j in no_dublicat_dict.items():
        a1, a2 = i.split(' - ')
        try:
            final_dict[a1][a2] = j
            final_dict[a2][a1] = j
        except:
            pass

    temp_team_list = list(final_dict.keys())
    result_dict = copy.deepcopy(final_dict)
    for i, j in final_dict.items():
        result_dict[i].update({'others': 0})
        for e, emp in j.items():
            if not e in temp_team_list:
                result_dict[i]['others'] += emp
                result_dict[i].pop(e)



    team_dict.update({t: result_dict})


