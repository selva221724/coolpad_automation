import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import json
# from datetime import datetime
import pytz

timezone = pytz.timezone('America/Chicago')
import requests

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
r = requests.get(tyson)
data = r.json()


# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})


# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
# startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
# endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
# current_date = datetime.datetime(2020, 9, 25, 0, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def dateTOtimestamp(startdate):
    d, t = startdate.split(' ')
    d = d.split('-')
    t = t.split('-')
    event_time = datetime.datetime(int(d[0]), int(d[1]), int(d[2]),
                                   hour=int(t[0]), minute=int(t[1]), second=int(t[2]))

    startTime = timezone.localize(event_time)
    return startTime

def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


data, start_date, end_date, startTime, endTime, = data, '2020-10-11 00-00-00', '2020-10-18 00-00-00', \
                                                  '2020-10-15 00-00-00', '2020-10-15 23-59-59'

data, start_date, end_date, startTime, endTime, = data, '2020-10-11 00-00-00', '2020-10-18 00-00-00', \
                                                  '2020-10-15 08-53-00', '2020-10-15 08-54-00'


def team_history(request, start_date, end_date, startTime, endTime, regions):
    data = default_getall(regions)

    start_date = dateTOtimestamp(start_date)
    end_date = dateTOtimestamp(end_date)
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)


    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})


    # =================================== day all =========================================

    max_pair1 = {}
    for i, j in data.items():

        sliced_events = []
        contact_events = j['contact_event']
        # if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
        max_pair1.update({i: sliced_events})

    pair_count1 = {}
    for i, j in max_pair1.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({emp_name: {'count': 0, 'max_duration': []}})
        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1
            temp[remote]['max_duration'].append(event['duration'])

        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count1.update({i: temp})

    target_dict = copy.deepcopy(pair_count1)
    f = copy.deepcopy(pair_count1)

    for k in pair_count1.keys():
        for i, j in f[k].items():
            temp_user = i
            temp_user_count = j['count']
            if not i == k:
                user_count = pair_count1[i][k]['count']
                if user_count > temp_user_count:
                    target_dict[k][i]['count'] = user_count
                    target_dict[k][i]['max_duration'] = pair_count1[i][k]['max_duration']

    temp_target = copy.deepcopy(target_dict)
    for i, j in temp_target.items():
        for k in j.keys():
            dev_id = users_device_id[k]
            target_dict[i][dev_id] = target_dict[i].pop(k)

    # =================================== week all =========================================

    max_pair2 = {}
    for i, j in data.items():

        sliced_events = []
        contact_events = j['contact_event']
        # if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(start_date, end_date, event_time):
                sliced_events.append(event)
        max_pair2.update({i: sliced_events})

    pair_count2 = {}
    for i, j in max_pair2.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({emp_name: {'count': 0, 'max_duration': []}})
        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1
            temp[remote]['max_duration'].append(event['duration'])

        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count2.update({i: temp})

    target_dict2 = copy.deepcopy(pair_count2)
    f2 = copy.deepcopy(pair_count2)

    for k in pair_count2.keys():
        for i, j in f2[k].items():
            temp_user = i
            temp_user_count = j['count']
            if not i == k:
                user_count = pair_count2[i][k]['count']
                if user_count > temp_user_count:
                    target_dict2[k][i]['count'] = user_count
                    target_dict2[k][i]['max_duration'] = pair_count2[i][k]['max_duration']

    temp_target2 = copy.deepcopy(target_dict2)
    for i, j in temp_target2.items():
        for k in j.keys():
            dev_id = users_device_id[k]
            target_dict2[i][dev_id] = target_dict2[i].pop(k)

    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})

        # ========================== merger pair count =========================================
        temp_pair = pair_count
        for u, k in temp_pair.items():
            for i, j in k.items():
                count_1 = j
                count_tar = target_dict[u][i]['count']
                if count_tar > count_1:
                    pair_count[u][i] = count_tar

        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)
        # ======================== no of users in contact day ==================
        count_dict = {}
        for i, j in result_dict.items():
            for k, v in j.items():
                if not v == 0:
                    if k in count_dict:
                        count_dict[k] += v
                    else:
                        count_dict.update({k: v})
        no_of_users_in_contact = len(count_dict)
        # ========================= no of contacts in a day =============================
        no_of_contacts = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts += j
        # ========================== Users with maximum contact in a day ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))
        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a day ======================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_day = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_day = {' ': 0}
        except:
            most_contact_user_pair_day = {' ': 0}

        # ============================================================================================
        # ================================== week wise =====2444=========================================
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(start_date, end_date, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        # ========================== merger pair count =========================================
        temp_pair = pair_count
        for u, k in temp_pair.items():
            for i, j in k.items():
                count_1 = j
                count_tar = target_dict2[u][i]['count']
                if count_tar > count_1:
                    pair_count[u][i] = count_tar
        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)

        # ========================= no of contacts in a week =============================
        no_of_contacts_week = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts_week += j
        # ========================== Users with maximum contact in a week ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))

        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact_week = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a week =====================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_week = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_week = {' ': 0}
        except:
            most_contact_user_pair_week = {' ': 0}
        # =============================== Final dict append =========================
        team_dict.update({t: {
            'Users in Contact': no_of_users_in_contact,
            'Contacts in a Day': no_of_contacts,
            'Contacts in a Week': no_of_contacts_week,
            'top 5 users in contact Day': top_five_users_max_no_contact,
            'top 5 users in contact Week': top_five_users_max_no_contact_week,
            'Most contact user pair Day': most_contact_user_pair_day,
            'Most contact user pair Week': most_contact_user_pair_week
        }})

    return Response(team_dict)


print(team_dict['Team A'])
# return Response(team_dict)

