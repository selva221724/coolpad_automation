import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import copy
import datetime
import json
import random

import dateutil.relativedelta
import pytz
from natsort import natsorted

timezone = pytz.timezone('America/Chicago')
import requests

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
hq = "https://takvaviya.in/coolpad_backend/user/getall/hq__America__South__Coolpad?format=json"
r = requests.get(tyson)
data = r.json()

#
# with open(
#         '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
#     data = json.load(f)

# ================================ Get Users vs device id ===============================


users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})



def dateTOtimestamp(startdate):
    d, t = startdate.split(' ')
    d = d.split('-')
    t = t.split('-')
    event_time = datetime.datetime(int(d[0]), int(d[1]), int(d[2]),
                                   hour=int(t[0]), minute=int(t[1]), second=int(t[2]))

    startTime = timezone.localize(event_time)
    return startTime


# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================

def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    distance = round(float(distance), 2)
    distance = str(distance) + ' ft'
    # print(distance, "thambi")
    return distance


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


primery_target = 'T-1'


startTime, endTime, = '2020-10-18 00-00-00', '2020-10-18 23-59-59'



def get_top_five_tree(data, primery_target, startTime, endTime, users_device_id, add_event=None):
    max_pair = {}
    for i, j in data.items():

        sliced_events = []
        contact_events = j['contact_event']
        # if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
        max_pair.update({i: sliced_events})

    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            if add_event:
                temp.update({emp_name: {'count': 0, 'max_duration': [], 'Date': [], 'events': []}})
            else:
                temp.update({emp_name: {'count': 0, 'max_duration': [], 'Date': []}})

        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1
            temp[remote]['max_duration'].append(event['duration'])
            temp[remote]['Date'].append(event['start'])
            if add_event:
                temp[remote]['events'].append(event)

        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None

        for t, tt in temp.items():
            try:
                start_date = min(tt['Date'])
                temp[t]['Date'] = datetime.datetime.utcfromtimestamp(start_date / 1000)
            except:
                temp[t]['Date'] = None

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count.update({i: temp})

    target_dict = {primery_target: pair_count[primery_target]}

    f = copy.deepcopy(target_dict)

    for i, j in f[primery_target].items():
        temp_user = i
        temp_user_count = j['count']
        if not i == primery_target:
            user_count = pair_count[i][primery_target]['count']
            if user_count > temp_user_count:
                target_dict[primery_target][i]['count'] = user_count
                target_dict[primery_target][i]['max_duration'] = pair_count[i][primery_target]['max_duration']
                target_dict[primery_target][i]['Date'] = pair_count[i][primery_target]['Date']

                if add_event:
                    # =============================== contact event all ===================================
                    contact_event_all = []
                    for eve in pair_count[i][primery_target]['events']:
                        remote = eve['local']
                        remote = [a for a, v in users_device_id.items() if v == remote][0]
                        time_st = datetime.datetime.utcfromtimestamp(eve['start'] / 1000)
                        time_st = time_st.replace(tzinfo=pytz.utc).astimezone(timezone)
                        time_st = time_st.strftime("%I:%M %p  %D")
                        avgdist = int(eve['avgDist']) / 10
                        minDist = int(eve['minDist']) / 10
                        avgdist = cm_to_ft(avgdist)
                        minDist = cm_to_ft(minDist)
                        temp_dict = {'timestamp': time_st, 'duration': ms_to_time(eve['duration']),
                                     'milliseconds': eve['duration'],
                                     'avgDist': avgdist, 'minDist': minDist}

                        contact_event_all.append(temp_dict)

                    target_dict[primery_target][i]['events'] = contact_event_all

            else:
                if add_event:
                    # =============================== contact event all ===================================
                    contact_event_all = []
                    for eve in target_dict[primery_target][i]['events']:
                        remote = eve['remote']
                        remote = [a for a, v in users_device_id.items() if v == remote][0]
                        time_st = datetime.datetime.utcfromtimestamp(eve['start'] / 1000)
                        time_st = time_st.replace(tzinfo=pytz.utc).astimezone(timezone)
                        time_st = time_st.strftime("%I:%M %p  %D")
                        avgdist = int(eve['avgDist']) / 10
                        minDist = int(eve['minDist']) / 10
                        avgdist = cm_to_ft(avgdist)
                        minDist = cm_to_ft(minDist)
                        temp_dict = {'timestamp': time_st, 'duration': ms_to_time(eve['duration']),
                                     'milliseconds': eve['duration'],
                                     'avgDist': avgdist, 'minDist': minDist}
                        contact_event_all.append(temp_dict)

                    target_dict[primery_target][i]['events'] = contact_event_all

    top_five = dict(list(list(target_dict.values())[0].items()))
    result_dict = copy.deepcopy(top_five)
    for i, j in top_five.items():
        if j['count'] == 0:
            result_dict.pop(i)

    for i, j in result_dict.items():
        team = data[i]['team']
        result_dict[i].update({'team': team})

    return {primery_target: result_dict}


def contact_trace(data, primery_target, startTime, endTime, users_device_id):
    startTime, endTime = dateTOtimestamp(startTime), dateTOtimestamp(endTime)

    final_list = []

    top_five = get_top_five_tree(data, primery_target, startTime, endTime, users_device_id, add_event=True)
    final_list.append(top_five)
    # tree_dict = copy.deepcopy(top_five)

    for i in top_five.values():
        temp = []
        for j, k in i.items():
            primery_target = j
            top_five = get_top_five_tree(data, primery_target, startTime, endTime, users_device_id)
            temp.append(top_five)

    final_list.append(temp)
    return final_list


# startTime, endTime = dateTOtimestamp(startTime), dateTOtimestamp(endTime)
trace = contact_trace(data, primery_target, startTime, endTime, users_device_id)

