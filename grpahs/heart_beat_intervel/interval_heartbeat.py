import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz

timezone = pytz.timezone('America/Chicago')


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


event_1 = {"local": "90030000783690102202", "tstamp": 1600805480993, "battery": 1000, "voltage": 4187, "interval": 3600}

N = 2

event_time_1 = datetime.datetime.utcfromtimestamp(event_1['tstamp'] / 1000)
event_time_1 = event_time_1.replace(tzinfo=pytz.utc).astimezone(timezone)
expected_time = event_time_1 + datetime.timedelta(seconds=event_1['interval']*N)
timestap_ui = event_time_1.strftime("%I:%M %p  %D")

current_time = datetime.datetime.now().astimezone(timezone)
# expected_time  =current_time + datetime.timedelta(seconds=60)


time_diff = current_time - expected_time
if time_diff.days < 0:
    print('ON')
elif time_diff.days == 0:
    print('Missing')
