import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})


#
# users_device_id = dict(emp_1='023456789A123456789B', emp_2='123456789A123456789B', emp_3='223456789A123456789B',
#                        emp_4='323456789A123456789B', emp_5='423456789A123456789B', emp_6='523456789A123456789B',
#                        emp_7='623456789A123456789B', emp_8='723456789A123456789B', emp_9='823456789A123456789B',
#                        emp_10='923456789A123456789B', emp_11='102345679A123456789B', emp_12='112345789A123456789B',
#                        emp_13='1223456789A13456789B', emp_14='1323456789A23456789B', emp_15='1423456789123456789B',
#                        emp_16='152346789A123456789B', emp_17='1623456789A13456789B', emp_18='1723456789A12345689B',
#                        emp_19='1823456789A13456789B', emp_20='192345679A123456789B')

# ========================================== delete user from the data =======================

def delete_user(data, target_user):
    duplicate = copy.deepcopy(data)
    duplicate.pop(target_user)
    device_id = users_device_id[target_user]
    for i, j in duplicate.items():
        old_list = duplicate[i]['contact_event']
        for c_event in old_list:
            if c_event['remote'] == device_id:
                duplicate[i]['contact_event'].remove(c_event)

    return duplicate


del_data = delete_user(data, target_user='emp_2')

# ============================================ replace id ==========================================

def repalce_device_id(data, target_user, new_id):
    old_id = users_device_id[target_user]

    duplicate = copy.deepcopy(data)
    duplicate[target_user]['device_id'] = new_id
    old_list = duplicate[target_user]['contact_event']
    for i in old_list:
        i['local'] = new_id

    duplicate[target_user]['contact_event'] = old_list
    duplicate[target_user]['history_of_ids'].append(old_id)

    for i, j in duplicate.items():
        old_list = duplicate[i]['contact_event']
        new_list = []
        for c_event in old_list:
            if c_event['remote'] == old_id:
                c_event['remote'] = new_id
                new_list.append(c_event)
            else:
                new_list.append(c_event)

        duplicate[i]['contact_event'] = new_list

    return duplicate


replace_data = repalce_device_id(data, target_user='emp_2', new_id="123456789A12345ZZZZZ")
