import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
from statistics import mean
import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz

timezone = pytz.timezone('America/Chicago')

with open(
        'data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

a = [{"local":"90030000783690102203","remote":"90030000783690102204","start":1601424813821,"duration":300181,"avgDist":218,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601423911445,"duration":301098,"avgDist":215,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601425714792,"duration":300377,"avgDist":220,"minDist":209},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601424212543,"duration":300104,"avgDist":214,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601425414591,"duration":300201,"avgDist":222,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601425114002,"duration":300589,"avgDist":232,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601426015169,"duration":301147,"avgDist":215,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601427216682,"duration":300081,"avgDist":222,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601426316316,"duration":300146,"avgDist":225,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601426616462,"duration":300176,"avgDist":217,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601424512647,"duration":301174,"avgDist":221,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601426916638,"duration":300044,"avgDist":251,"minDist":212},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601427816792,"duration":300374,"avgDist":232,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601428417250,"duration":300080,"avgDist":218,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601428117166,"duration":300084,"avgDist":223,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601429918971,"duration":299962,"avgDist":222,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601429618790,"duration":300181,"avgDist":216,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601429018706,"duration":299489,"avgDist":226,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601427516763,"duration":300029,"avgDist":216,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601428717330,"duration":301376,"avgDist":218,"minDist":209},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601429318195,"duration":300595,"avgDist":229,"minDist":211},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601430218933,"duration":301282,"avgDist":224,"minDist":209},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601430520215,"duration":300071,"avgDist":222,"minDist":210},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601430820286,"duration":300875,"avgDist":216,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601431421671,"duration":300630,"avgDist":216,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601432924074,"duration":300684,"avgDist":221,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601431121161,"duration":300510,"avgDist":214,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601434125590,"duration":300598,"avgDist":224,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601432623977,"duration":300097,"avgDist":211,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601431722301,"duration":300600,"avgDist":215,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601433824645,"duration":300945,"avgDist":214,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601433224758,"duration":298604,"avgDist":222,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601432022901,"duration":300768,"avgDist":214,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601432323669,"duration":300308,"avgDist":221,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601433523362,"duration":301283,"avgDist":222,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601434426188,"duration":300497,"avgDist":214,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601435929593,"duration":299935,"avgDist":222,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601436830262,"duration":300402,"avgDist":212,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601436229528,"duration":300611,"avgDist":233,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601436530139,"duration":300123,"avgDist":236,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601437731320,"duration":300387,"avgDist":240,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601434726685,"duration":299882,"avgDist":212,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601438031707,"duration":299702,"avgDist":245,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601435327603,"duration":300519,"avgDist":216,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601435026567,"duration":301036,"avgDist":213,"minDist":205},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601437130664,"duration":300316,"avgDist":217,"minDist":204},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601435628122,"duration":301471,"avgDist":212,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601437430980,"duration":300340,"avgDist":209,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601439233436,"duration":299403,"avgDist":220,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601438932087,"duration":301349,"avgDist":220,"minDist":219},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601438331409,"duration":300612,"avgDist":219,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601441035783,"duration":300260,"avgDist":220,"minDist":219},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601438632021,"duration":300066,"avgDist":320,"minDist":218},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601439532839,"duration":301241,"avgDist":221,"minDist":219},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601439834080,"duration":300173,"avgDist":221,"minDist":219},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601440134253,"duration":300885,"avgDist":220,"minDist":218},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601440435138,"duration":299848,"avgDist":236,"minDist":217},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601441638181,"duration":299212,"avgDist":223,"minDist":216},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601440734986,"duration":300797,"avgDist":254,"minDist":220},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601441336043,"duration":302138,"avgDist":234,"minDist":219},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601442238648,"duration":300595,"avgDist":221,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601441937393,"duration":301255,"avgDist":214,"minDist":212},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601444039983,"duration":299892,"avgDist":218,"minDist":210},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601442539243,"duration":299726,"avgDist":217,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601442838969,"duration":299104,"avgDist":217,"minDist":210},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601444940298,"duration":300487,"avgDist":216,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601443738700,"duration":301283,"avgDist":217,"minDist":206},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601443138073,"duration":301684,"avgDist":216,"minDist":207},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601444339875,"duration":300231,"avgDist":232,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601445240785,"duration":300397,"avgDist":216,"minDist":208},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601443439757,"duration":298943,"avgDist":216,"minDist":209},{"local":"90030000783690102203","remote":"90030000783690102204","start":1601444640106,"duration":300192,"avgDist":216,"minDist":205}]

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
def dateTOtimestamp(startdate):
    t = startdate.split('-')
    event_time = datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
    startTime = event_time.replace(tzinfo=timezone).astimezone(timezone)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


current_date = '2020-09-30'
sp = current_date.split('-')
current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)


for em, emp in data.items():

    contact_events = emp['contact_event']

    sliced_events = []
    if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if current_date.date() == event_time.date():
                sliced_events.append(event)

    events = copy.deepcopy(sliced_events)
    if not events == []:
        # events = [
        #     {"local": "8912230200020077083f", "remote": "89011703278216971203", "start": 1596748151164,
        #      "duration": 300000,
        #      "avgDist": 1280, "minDist": 762},
        #     {"local": "8912230200020077083f", "remote": "89011703278216971203", "start": 1596748156164,
        #      "duration": 135000,
        #      "avgDist": 1280, "minDist": 762}]

        duplicate_event = events.copy()
        for i in events:
            local = i['local']
            remote = i['remote']
            start = i['start']
            temp_events = []

            for j in events:
                local_temp = j['local']
                remote_temp = j['remote']
                start_temp = j['start']
                if local == local_temp and remote == remote_temp:
                    time_dif = datetime.datetime.utcfromtimestamp(
                        start_temp / 1000) - datetime.datetime.utcfromtimestamp(start / 1000)
                    time_dif = time_dif.total_seconds()
                    time_dif = abs(time_dif)
                    if not time_dif == 0.0:
                        if time_dif < 330:
                            temp_events.append(j)

            if not temp_events == []:
                avgdist = []
                mindist = []
                duration_time = []
                start_time = []
                temp_events.append(i)

                for t in temp_events:
                    avgdist.append(t['avgDist'])
                    mindist.append(t['minDist'])
                    duration_time.append(t['duration'])
                    start_time.append(t['start'])
                    try:
                        duplicate_event.remove(t)
                    except Exception as e:
                        pass

                one_event = temp_events[0]
                merge_event = {"local": one_event['local'], "remote": one_event['remote'], "start": min(start_time),
                               "duration": sum(duration_time),
                               "avgDist": int(mean(avgdist)), "minDist": min(mindist)}

                if merge_event not in duplicate_event:
                    duplicate_event.append(merge_event)

        data[em]['contact_event'] = duplicate_event
