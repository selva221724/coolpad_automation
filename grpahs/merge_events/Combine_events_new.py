import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
from statistics import mean
import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz

a = [{'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601442238648, 'duration': 300595,
      'avgDist': 221, 'minDist': 206},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601444039983, 'duration': 299892,
      'avgDist': 218, 'minDist': 210},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601442539243, 'duration': 299726,
      'avgDist': 217, 'minDist': 207},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601442838969, 'duration': 299104,
      'avgDist': 217, 'minDist': 210},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601444940298, 'duration': 300487,
      'avgDist': 216, 'minDist': 208},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601443738700, 'duration': 301283,
      'avgDist': 217, 'minDist': 206},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601443138073, 'duration': 301684,
      'avgDist': 216, 'minDist': 207},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601444339875, 'duration': 300231,
      'avgDist': 232, 'minDist': 208},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601445240785, 'duration': 300397,
      'avgDist': 216, 'minDist': 208},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601443439757, 'duration': 298943,
      'avgDist': 216, 'minDist': 209},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601444640106, 'duration': 300192,
      'avgDist': 216, 'minDist': 205},

     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601445840455, 'duration': 300192,
      'avgDist': 216, 'minDist': 205},
     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601446140000, 'duration': 300192,
      'avgDist': 216, 'minDist': 205},

     {'local': '90030000783690102203', 'remote': '90030000783690102204', 'start': 1601494640206, 'duration': 300192,
      'avgDist': 216, 'minDist': 205}

     ]
a = sorted(a, key=lambda x: x['start'])

timezone = pytz.timezone('America/Chicago')

with open(
        'data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})


# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
def dateTOtimestamp(startdate):
    t = startdate.split('-')
    event_time = datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
    startTime = event_time.replace(tzinfo=timezone).astimezone(timezone)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


#
# current_date = '2020-09-30'
# sp = current_date.split('-')
# current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
# current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)

contact_events = data['E-9']['contact_event']

def merge_events(contact_events):

    remote_users = []
    for i in contact_events:
        if not i['remote'] in remote_users:
            remote_users.append(i['remote'])

    temp_events = []
    for i in remote_users:
        temp = []
        for j in contact_events:
            if j['remote'] == i:
                temp.append(j)

        temp = sorted(temp, key=lambda x: x['start'])
        temp_events.append(temp)

    comb_all = []
    for i in temp_events:
        temp_event = i
        dup_event = copy.deepcopy(i)
        comb_event = []
        temp_comb = [temp_event[0]]

        for e in range(1, len(temp_event)):
            ts_1 = temp_event[e - 1]['start']
            duration_1 = temp_event[e - 1]['duration']

            buffer_seconds = 10
            ts_1 = datetime.datetime.utcfromtimestamp(int(ts_1) / 1000)
            ts_1 = ts_1.replace(tzinfo=pytz.utc).astimezone(timezone)
            exp_time = ts_1 + datetime.timedelta(seconds=int(duration_1 / 1000) + buffer_seconds)

            ts_2 = temp_event[e]['start']
            duration_2 = temp_event[e]['duration']
            ts_2 = datetime.datetime.utcfromtimestamp(int(ts_2) / 1000)
            ts_2 = ts_2.replace(tzinfo=pytz.utc).astimezone(timezone)

            tim_diff = exp_time - ts_2

            if tim_diff.days == 0 and tim_diff.seconds < 15:
                temp_comb.append(temp_event[e])
            else:
                comb_event.append(temp_comb)
                temp_comb = [temp_event[e]]

        comb_event.append(temp_comb)
        comb_all.append(comb_event)

    final_events = []
    for eve in comb_all:
        for i in eve:
            if len(i) == 0:
                final_events.append(i)
            else:
                duration = []
                avg_dist = []
                min_dist = []
                for j in i:
                    duration.append(j['duration'])
                    avg_dist.append(j['avgDist'])
                    min_dist.append(j['minDist'])

                t = i[0]
                t['duration'] = sum(duration)
                t['avgDist'] = int(mean(avg_dist))
                t['minDist'] = int(mean(min_dist))
                final_events.append(t)

    return final_events

f = merge_events(contact_events)