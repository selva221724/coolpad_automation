import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 12, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def outliers(data, startTime, endTime, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        a = ' - '.join(a)
        no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    top_pairs =  dict(list(top_pairs.items())[:2])

    result_dict = {}
    for i, j in top_pairs.items():
        e1, e2 = i.split(' - ')
        result_dict.update({e1: j})
        result_dict.update({e2: j})

    return result_dict


outlier_users = outliers(data, startTime, endTime, users_device_id)

# ================================= gencsv, excel and  pdf ========================================================

import pandas as pd
import csv

df = pd.DataFrame(outlier_users.items(), columns=['Employees Pair', 'Count'])# df = df.T

df.to_csv('Outliers.csv', index=False)

df.to_excel('Outliers.xlsx', index=False)

from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet

startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def pdf_gen(csv_file, current_date):
    # Introduction text
    line1 = 'Outliers Report'
    line2 = 'Date: ' + current_date.strftime("%B %d %Y")

    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    # File that must be written to report
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)

    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    elements = []
    archivo_pdf = SimpleDocTemplate('Outliers.pdf', pagesize=A4, rightMargin=30, leftMargin=30, topMargin=40,
                                    bottomMargin=28)

    im = Image('logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)

    archivo_pdf.build(elements)


pdf_gen('Outliers.csv', current_date)