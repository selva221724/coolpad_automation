import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz
timezone = pytz.timezone('America/Chicago')
# IST = pytz.timezone('Asia/Calcutta')

with open(
        'data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair =====================
current_date = datetime.datetime(2020, 9, 23, 00, 00, 00)
#
# from datetime import datetime
# import pytz
# IST = pytz.timezone('Asia/Kolkata')

def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a

def top_pairs_maxmium_contact(data, current_date, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                # event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        if not a[0] == a[1]:
            a = ' - '.join(a)
            no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    return top_pairs


top_pairs = top_pairs_maxmium_contact(data, current_date, users_device_id)

# ==============================================================================================
# ================================= All contact pair count =====================



def pairs_maxmium_contact(data, current_date, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        if not a[0] == a[1]:
            a = ' - '.join(a)
            no_dublicat_dict.update({a: j})

    return no_dublicat_dict


pairs_history = pairs_maxmium_contact(data, current_date, users_device_id)


# ==============================================================================================
# ================================== Total Number of Contacts Today ======================

# current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def total_no_of_contacts(data, current_date):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================Duplicate removal  ============================

    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = local_time - remote_time
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)

    result = len(all_events)

    return result


total_no_contacts = total_no_of_contacts(data, current_date)


# ==============================================================================================
# ===================================== Contact freqency matrix 40x40 ============================

# current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def freqency_matrix(data, current_date, users_device_id):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict.update({t: result_dict})

    return team_dict


freq_matrx = freqency_matrix(data, current_date, users_device_id)

# ======================================================================================================
# ===========================  Last five minutes live ==================================================


# mints_back = current_time - datetime.timedelta(minutes=5)


# in original case:
# current_time = datetime.datetime.now()
# mints_back = current_time - datetime.timedelta(minutes=5)





def last_5_mints(data, users_device_id):
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            if isNowInTimePeriod(mints_back, current_date, event_time):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})

    live_list = []
    for i in all_events:
        temp = {}
        local = [k for k, v in users_device_id.items() if v == i['local']][0]
        remote = [k for k, v in users_device_id.items() if v == i['remote']][0]
        event_time = datetime.datetime.utcfromtimestamp(i['start'] / 1000)
        event_time = event_time.strftime("%I:%M:%S %p")
        avgdist = str(int(i['avgDist'] / 10)) + ' cm'
        minDist = str(int(i['minDist'] / 10)) + ' cm'
        temp.update({'local': local, 'remote': remote, 'start': event_time, 'duration': ms_to_time(i['duration']),
                     'avgDist': avgdist, 'minDist': minDist})
        live_list.append(temp)

    return live_list


# last5mints = last_5_mints(data, users_device_id)


# ========================================= clock chart =================================================

# current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)

def clk_chart(data, current_date):
    current_date = datetime.datetime.now(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    # current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 00, 00)
    # current_date = current_date.replace(tzinfo=IST).astimezone(timezone)

    hours_list = []
    hours_list.append(current_date)
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)

    hour_wise_count = []
    for hr in range(len(hours_list) - 1):

        mints_back = hours_list[hr]
        current_date = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                    if isNowInTimePeriod(mints_back, current_date, event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})

        # ================================Duplicate removal  ============================

        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = local_time - remote_time
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)

        result = len(all_events)
        hour_wise_count.append(result)

    return hour_wise_count


clock_chart = clk_chart(data, current_date)


def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    return round(float(distance), 4)

# ============================== gen pdf for daily graph ===========================
#
#
#
# from reportlab.lib import colors
# from reportlab.lib.pagesizes import inch, letter, A4
# from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle, Image
# from reportlab.lib.styles import getSampleStyleSheet
#
# startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
# endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
# current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)
#
#
# def gen_pdf_day_wise(current_date,total_no_contacts,top_pairs,pairs_history,out):
#     # Introduction text
#     line1 = 'Daily Tracker Report'
#     line2 = 'Date: ' + current_date.strftime("%B %d %Y")
#     line3 = 'Total no of contacts: '+ str(total_no_contacts)
#     line4 = 'Top Contact History (Top 5)'
#     line5 = 'Contact History for All Users'
#
#     # PDF document layout
#     table_style = TableStyle([
#         ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
#         ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
#         ('VALIGN', (0, 0), (0, -1), 'CENTER'),
#         ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
#         ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
#         ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
#         ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
#         ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
#         ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
#     ])
#
#     styles = getSampleStyleSheet()
#     styleNormal = styles['Normal']
#     styleHeading = styles['Heading1']
#     styleHeading2 = styles['Heading2']
#     styleHeading.alignment = 1
#     styleHeading2.alignment = 1  # centre text (TA_CENTRE)
#
#     # Configure style and word wrap
#     s = getSampleStyleSheet()
#     s = s["BodyText"]
#     s.wordWrap = 'CJK'
#
#     lista =[]
#     lista.append(['Employees Pair',  'Count'])
#     for i,j in top_pairs.items():
#         lista.append([i,str(j)])
#
#     data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
#     t = Table(data2)
#     t.setStyle(table_style)
#     # t._argW[0] = 0.7 * inch
#
#
#     listb =[]
#     listb.append(['Employees Pair',  'Count'])
#     for i,j in pairs_history.items():
#         listb.append([i,str(j)])
#     data3 = [[Paragraph(cell, s) for cell in row] for row in listb]
#     t2 = Table(data3)
#     t2.setStyle(table_style)
#
#
#
#     elements = []
#     archivo_pdf = SimpleDocTemplate(out, pagesize=A4, rightMargin=30, leftMargin=30, topMargin=40,
#                                     bottomMargin=28)
#
#     im = Image('logo.png', width=230, height=100)
#     elements.append(im)
#     elements.append(Spacer(inch, .25 * inch))
#     if line1 is not None:
#         elements.append(Paragraph(line1, styleHeading))
#     elements.append(Paragraph(line2, styleHeading2))
#     elements.append(Spacer(inch, .25 * inch))
#     elements.append(Paragraph(line3, styleNormal))
#     elements.append(Spacer(inch, .25 * inch))
#     elements.append(Paragraph(line4, styleNormal))
#     elements.append(t)
#     elements.append(Spacer(inch, .25 * inch))
#     elements.append(Paragraph(line5, styleNormal))
#     elements.append(t2)
#
#     archivo_pdf.build(elements)
#
#
#
#
# gen_pdf_day_wise(current_date,total_no_contacts,top_pairs,pairs_history,'Daily Tracker.pdf')