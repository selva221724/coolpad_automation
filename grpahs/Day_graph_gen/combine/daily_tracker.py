def daliy_tracker(request, current_date, regions):
    # current_date = endTime.replace(tzinfo=timezone).astimezone(timezone)
    print(current_date)
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 12, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        if not a[0] == a[1]:
            a = ' - '.join(a)
            no_dublicat_dict.update({a: j})
    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    # return Response(top_pairs)

    # ================================total number of contacts ============================

    duplicate_events=copy.deepcopy(all_events)
    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    duplicate_events.remove(j)
    result = len(duplicate_events)
    # return Response(result)

    # ============================================== clk chart ========================
    current_date = datetime.datetime.now(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    # current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)
    # current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 00, 00)
    hours_list = []
    hours_list.append(current_date)
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod(mints_back, current_date_1, event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})
        # ================================Duplicate removal  ============================
        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)
        result = len(all_events)
        hour_wise_count.append(result)
    # return Response(hour_wise_count)

    combined_daily_tracker = {'top_pairs_maxmium_contact': top_pairs,
                              'total_no_of_contacts': result,
                              'clk_chart': hour_wise_count

                              }

    return Response(combined_daily_tracker)
