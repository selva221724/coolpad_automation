from rest_framework.generics import ListAPIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authentication import authenticate
from rest_framework.authentication import get_user_model
from .models import UserProfile
from rest_framework.generics import UpdateAPIView
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import authentication_classes, permission_classes
from .models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from django.contrib.auth.models import User, Group

from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet
# from datetime import datetime
import datetime
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
import pandas as pd
import csv
import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
from statistics import mean
from empNote.models import *
from rest_framework.permissions import IsAuthenticated
import json
from datetime import datetime
import pytz

timezone = pytz.timezone('America/Chicago')


@api_view(['POST'])
@authentication_classes([])
def create_user(request):
    print(request.data)
    data = request.data
    employee = UserProfile()
    employee.user = data.get('user')
    employee.user_id = data.get('user_id')
    employee.device_id = data.get('device_id')
    employee.locations = data.get('locations')
    employee.company = data.get('company')
    employee.zone = data.get('zone')
    employee.team = data.get('team')
    employee.group = data.get('group')
    k = []
    k.append(data.get('device_id'))
    employee.history_of_ids = json.dumps(k)

    employee.save()
    return Response({'status': 'success'})


@api_view(['POST'])
@authentication_classes([])
def updateUser(request,username,location):
    request = request.data
    data = default_getall(location)
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(location)
    emp = UserProfile.objects.filter(user=username).filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    for i in emp:
        old_dev_id=i.device_id
        requested_id=request['device_id']
        prev_team=i.team
        requested_team=request['team']
        if old_dev_id != requested_id:
            i.device_id = request['device_id']
            t = json.loads(i.history_of_ids)
            t.append(request['device_id'])
            i.history_of_ids = json.dumps(t)
            print(i.device_id,i.history_of_ids)
        if prev_team != requested_team:
            i.team = request['team']
            print(i.team)
        i.save()
        if old_dev_id != requested_id:
            a = replace_device_id(location, username, requested_id, data)
    return Response({'status': 'success'})

@api_view(['POST'])
@authentication_classes([])
def editUsername(request,username,location):
    request = request.data
    data = default_getall(location)
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(location)
    emp = UserProfile.objects.filter(user=username).filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    for i in emp:
        prev_team = i.team
        requested_team = request['team']
        i.user_id = request['user_id']
        if prev_team != requested_team:
            i.team = request['team']
            print(i.team)
        i.save()
    return Response({'status': 'success'})

@api_view(['POST'])
@authentication_classes([])
def deleteUser(request, username):
    emp = UserProfile.objects.filter(user=username)
    emp.delete()
    return Response({'status': 'success'})


@api_view(["POST"])
@authentication_classes([])
def saveEmpnote(request):
    try:
        print(request.data)
        data = request.data
        userProfile = UserProfile.objects.get(device_id=data.get('user'))
        added_data = empNotes()
        added_data.user = userProfile
        added_data.date_time = data.get('date_time')
        added_data.location = data.get('location')
        added_data.notes = data.get('notes')
        added_data.save()
        return Response({"status": "success", "id": added_data.id})
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})
@api_view(["GET"])
@authentication_classes([])
def empallnotes(request, username, location):
    try:
        # uname = get_user_model().objects.get(username = username)
        sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(location)
        userProfile = UserProfile.objects.get(device_id=username,  group=sliced_group,
            locations=sliced_loc,
            zone=sliced_zone,
            company=sliced_company,
        )
        data = empNotes.objects.filter(user=userProfile).filter(location=location)
        print(data)
        resp = {}
        resp[str(username)] = {}
        for d in data:
            print(d.id, 'deiiii')
            resp[str(username)][d.id] = {'date_time': d.date_time, 'notes': d.notes}
        return Response(resp)
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})
@api_view(["POST"])
@authentication_classes([])
def deleteEmpnote(request, id):
    print(id)
    try:
        note = empNotes.objects.get(id=id)
        note.delete()
        return Response({"status": "success"})
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})
@api_view(["POST"])
@authentication_classes([])
def saveTeamnote(request):
    try:
        print(request.data)
        data = request.data
        added_data = teamNotes()
        added_data.team = data.get('team')
        added_data.date_time = data.get('date_time')
        added_data.location = data.get('location')
        added_data.notes = data.get('notes')
        added_data.save()
        return Response({"status": "success", "id": added_data.id})
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})
@api_view(["GET"])
@authentication_classes([])
def teamallnotes(request, team, location):
    try:
        data = teamNotes.objects.filter(team=team).filter(location=location)
        print(data)
        resp = {}
        resp[str(team)] = {}
        for d in data:
            resp[str(team)][d.id] = {'date_time': d.date_time, 'notes': d.notes}
        return Response(resp)
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})
@api_view(["POST"])
@authentication_classes([])
def deleteTeamnote(request, id):
    print(id)
    try:
        note = teamNotes.objects.get(id=id)
        note.delete()
        return Response({"status": "success"})
    except Exception as e:
        return Response({"status": "failed", "exception": str(e)})

@api_view(['POST'])
@authentication_classes([])
def contact(request):
    print(request.data)
    datas = request.data
    check_local = UserProfile.objects.filter(device_id=datas.get('local'))
    check_remote = UserProfile.objects.filter(device_id=datas.get('remote'))
    if check_local.count()!=0 and check_remote.count()!=0:
        for i in check_local:
            for j in check_remote:
                print(i.locations,j.locations , i.group , j.group , i.zone , j.zone , i.company,j.company)
                if i.locations == j.locations and i.group == j.group and i.zone == j.zone and i.company == j.company:
                    print("success")
                    t = json.loads(i.contact_event)
                    t.append(request.data)
                    res_arr = merge_events(t)
                    i.contact_event = json.dumps(res_arr)
                    i.save()
    return Response({"status": "success"})


@api_view(['POST'])
@authentication_classes([])
def heart(request):
    datas = request.data
    local_assign = UserProfile.objects.filter(device_id=datas.get('local'))
    for k in local_assign:
        t = json.loads(k.heartbeat_event)
        t.append(request.data)
        k.heartbeat_event = json.dumps(t)
        k.recent_heartbeat_event = json.dumps(request.data)
        k.save()

    return Response({"status": "success"})


# @api_view(['POST'])
# def heart(request):
#     datas = request.data
#     user_assign = UserProfile.objects.filter(device_id=datas.get('local'))
#     dt_object = datetime.fromtimestamp(datas.get('tstamp'))
#     date = dt_object.date()
#     alldates = []
#
#     for k in user_assign:
#         check1 = k.heartbeat_event
#
#     for key in json.loads(check1):
#         alldates.append(key)
#
#     for i in user_assign:
#         t = json.loads(i.heartbeat_event)
#         if str(date) in alldates:
#             t[str(date)].append(request.data)
#         else:
#             t[str(date)] = [request.data]
#
#         i.heartbeat_event = json.dumps(t)
#         i.recent_heartbeat_event = json.dumps(request.data)
#         i.save()
#     return Response({"status": "success"})


@api_view(['GET'])
@authentication_classes([])
def deviceON_OFF(request, deviceID):
    print(deviceID)
    userdata = UserProfile.objects.filter(device_id=deviceID)
    for k in userdata:
        event_1 = json.loads(k.recent_heartbeat_event)
        N = 2
        event_time_1 = datetime.datetime.utcfromtimestamp(event_1['tstamp'] / 1000)
        event_time_1 = event_time_1.replace(tzinfo=pytz.utc).astimezone(timezone)
        expected_time = event_time_1 + datetime.timedelta(seconds=event_1['interval'] * N)
        current_time = datetime.datetime.now().astimezone(timezone)
        time_diff = current_time - expected_time
        if time_diff.days < 0:
            status = "ON"
        elif time_diff.days >= 0:
            status = "MISSING"
        try:
            if event_1["shutdown"] == True:
                status = "OFF"
        except:
            print("error")
    return Response({"shutdown": status})


@api_view(['GET'])
def getall(request, regions):
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    resp = {}
    for k in alluser:
        resp[str(k)] = {
            'user': k.user,
            'user_id': k.user_id,
            'device_id': k.device_id,
            'team': k.team,
            'group': k.group,
            'locations': k.locations,
            'zone': k.zone,
            'company': k.company,
            'priviledge': k.priviledge,
            'history_of_ids': json.loads(k.history_of_ids),
            'contact_event': json.loads(k.contact_event),
            'heartbeat_event': json.loads(k.heartbeat_event),
            'recent_contact_event': json.loads(k.recent_contact_event),
            'recent_heartbeat_event': json.loads(k.recent_heartbeat_event),
            'is_Deleted': k.is_Deleted
        }
    return Response(resp)


@api_view(['GET'])
def device_status(request, regions):
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    resp = {}
    for k in alluser:
        resp[str(k)] = {
            'user': k.user,
            'user_id': k.user_id,
            'device_id': k.device_id,
            'team': k.team,
            'group': k.group,
            'locations': k.locations,
            'zone': k.zone,
            'company': k.company,
            'device_status': json.loads(k.recent_heartbeat_event),
            'is_Deleted': k.is_Deleted
        }
    return Response(resp)


def default_getall(regions):
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )

    resp = {}
    for k in alluser:
        resp[str(k)] = {
            'user': k.user,
            'user_id': k.user_id,
            'device_id': k.device_id,
            'team': k.team,
            'group': k.group,
            'locations': k.locations,
            'zone': k.zone,
            'company': k.company,
            'priviledge': k.priviledge,
            'history_of_ids': json.loads(k.history_of_ids),
            'contact_event': json.loads(k.contact_event),
            'heartbeat_event': json.loads(k.heartbeat_event),
            'recent_contact_event': json.loads(k.recent_contact_event),
            'recent_heartbeat_event': json.loads(k.recent_heartbeat_event),
        }
    return resp


import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta


@api_view(['GET'])
def top_pairs_maxmium_contact(request, current_date, regions):
    # current_date = endTime.replace(tzinfo=timezone).astimezone(timezone)
    print(current_date)
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 12, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)

    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    return Response(top_pairs)


@api_view(['GET'])
def freqency_matrix(request, current_date, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if current_date == str(event_time.date()):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
    # ================================ matching back the device id with user id  ================

    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    final_dict = {}
    for i, j in pair_count.items():
        temp = {}
        for emp, k in j.items():
            emp_id = [k for k, v in users_device_id.items() if v == emp][0]
            temp.update({emp_id: k})
        final_dict.update({i: temp})
        # return Response(final_dict)

    for i, j in no_dublicat_dict.items():
        a1, a2 = i.split(' - ')
        final_dict[a1][a2] = j
        final_dict[a2][a1] = j

    return Response(final_dict)


@api_view(['GET'])
def total_no_of_contacts(request, current_date, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================Duplicate removal  ============================
    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)
    result = len(all_events)
    return Response(result)


# in original case:
# current_time = datetime.datetime.now(timezone)
# mints_back = current_time - datetime.timedelta(minutes=5)
def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + "h " + str(minutes) + 'm ' + str(seconds) + "s"
    return a


@api_view(['GET'])
def userDeviceStatus(request, regions):
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    resp = {}
    for k in alluser:
        event_1 = json.loads(k.recent_heartbeat_event)
        if not event_1 == {}:
            N = 2
            event_time_1 = datetime.datetime.utcfromtimestamp(event_1['tstamp'] / 1000)
            event_time_1 = event_time_1.replace(tzinfo=pytz.utc).astimezone(timezone)
            expected_time = event_time_1 + datetime.timedelta(seconds=event_1['interval'] * N)
            time_stamp = event_time_1.strftime("%I:%M %p  %D")
            current_time = datetime.datetime.now().astimezone(timezone)
            time_diff = current_time - expected_time
            if time_diff.days < 0:
                print('ON')
                status = "ON"
            elif time_diff.days >= 0:
                print('Missing')
                status = "MISSING"

            try:
                if event_1["shutdown"] == True:
                    status = "OFF"
            except:
                print("error")
            print(time_stamp)
        else:
            time_stamp = "N/A"
            status = "N/A"
        resp[str(k)] = {
            'user': k.user,
            'user_id': k.user_id,
            'device_id': k.device_id,
            'team': k.team,
            'group': k.group,
            'locations': k.locations,
            'zone': k.zone,
            'company': k.company,
            'history_of_ids': json.loads(k.history_of_ids),
            'recent_heartbeat_event': json.loads(k.recent_heartbeat_event),
            'time_stamp': time_stamp,
            'device_status': status,
            'is_Deleted': k.is_Deleted
        }
    return Response(resp)


@api_view(['GET'])
def last_5_mints(request, regions):
    data = default_getall(regions)
    users_device_id = {}
    current_time = datetime.datetime.now(timezone)
    # current_time = datetime.datetime(2020, 8, 4, 12, 00, 00)
    print("hi", current_time)
    mints_back = current_time - datetime.timedelta(hours=2)
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    all_events = []

    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_time, event_time):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})
    live_list = []
    print(all_events, "nnnn")
    for i in all_events:
        temp = {}
        local = [k for k, v in users_device_id.items() if v == i['local']][0]
        remote = [k for k, v in users_device_id.items() if v == i['remote']][0]
        event_time = datetime.datetime.utcfromtimestamp(int(i['start']) / 1000)

        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        event_time = event_time.strftime("%I:%M:%S %p")
        avgdist = int(i['avgDist']) / 10
        minDist = int(i['minDist']) / 10

        avgdist = cm_to_ft(avgdist)
        minDist = cm_to_ft(minDist)
        temp.update({'local': local, 'remote': remote, 'start': event_time, 'duration': ms_to_time(i['duration']),
                     'avgDist': avgdist, 'minDist': minDist})
        live_list.append(temp)

    return Response(live_list)


def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    distance = round(float(distance), 2)
    distance = str(distance) + ' ft'
    print(distance, "thambi")
    return distance


# current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)
@api_view(['GET'])
def pairs_maxmium_contact(request, current_date, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    return Response(no_dublicat_dict)


@api_view(['GET'])
def clk_chart(request, regions):
    data = default_getall(regions)
    current_date = datetime.datetime.now(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    # current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)
    # current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 00, 00)
    hours_list = []
    hours_list.append(current_date)
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod(mints_back, current_date, event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})
        # ================================Duplicate removal  ============================
        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)
        result = len(all_events)
        hour_wise_count.append(result)
    return Response(hour_wise_count)


import copy


@api_view(['GET'])
def freqency_team(request, current_date, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        # ================================ Find the pair counts ================
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)
        team_dict.update({t: result_dict})
    return Response(team_dict)


@api_view(['GET'])
def top_pairs_maxmium_contact_week(request, startTime, endTime, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    all_events = []
    max_pair = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    return Response(top_pairs)


@api_view(['GET'])
def total_no_of_contacts_weekly(request, startTime, endTime, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    all_events = []
    max_pair = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================Duplicate removal  ============================

    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)

    result = len(all_events)

    return Response(result)


@api_view(['GET'])
def freqency_matrix_weekly(request, startTime, endTime, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

        final_dict = {}
        for i in max_device:
            first_emp = list(i.keys())[0]
            second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
            })

        # ================================Duplicate removal  ============================
        no_dublicat_dict = {}
        for i, j in final_dict.items():
            if not j == 0:
                a = i.split(' - ')
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in no_dublicat_dict:
                        if no_dublicat_dict[a] < j:
                            no_dublicat_dict.update({a: j})
                    else:
                        no_dublicat_dict.update({a: j})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        for i, j in no_dublicat_dict.items():
            a1, a2 = i.split(' - ')
            try:
                final_dict[a1][a2] = j
                final_dict[a2][a1] = j
            except:
                pass

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict.update({t: result_dict})

    return Response(team_dict)


@api_view(['GET'])
def weekly_team_tracker(request, startTime, endTime, regions):
    data = default_getall(regions)
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []

        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)

        # ================================Duplicate removal  ============================

        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)

        result = len(all_events)

        team_dict.update({t: result})

    return Response(team_dict)


@api_view(['GET'])
def pairs_history_weekly(request, startTime, endTime, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    return Response(no_dublicat_dict)


'''@api_view(['GET'])
def User_history_data(request,startTime, endTime, current_date,regions):
    data = default_getall(regions)

    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    # current_date = dateTOtimestamp(current_date)
    user_data = {}

    c = current_date
    for u, user in data.items():
        count = 0
        current_date = c
        device_id = user['device_id']
        contact_events = user['contact_event']

        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})


            max_device = []
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: 0})
                for event in j:
                    temp[event['remote']] += 1
                pair_count.update({i: temp})
                max_key = max(temp, key=lambda k: temp[k])
                max_device.append({i: {max_key: temp[max_key]}})
            # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
            # ========================= no of contacts in single day =============================
            no_of_contacts = 0
            for i in pair_count.values():
                for j in i.values():
                    if not j == 0:
                        no_of_contacts += 1
            # ============================== max contact duration with user ===================
            max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
            temp_max = {}
            for i in max_duration:
                temp_max.update({list(i.keys())[0]: list(i.values())[0]})
            try:
                max_user, max_duration = max(temp_max.items(), key=lambda k: k[1])
                max_user = [a for a, v in users_device_id.items() if v == max_user][0]
                max_duration_day = ms_to_time(max_duration) + '  ' + max_user
            except :
                max_duration_day=None
            # ============================= contact history single day  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None

                pair_count.update({i: temp})
            contact_history = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
        # =========================================================================================
        # ================================== week wise ============================================
        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})
            max_device = []
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: 0})
                for event in j:
                    temp[event['remote']] += 1
                pair_count.update({i: temp})
                max_key = max(temp, key=lambda k: temp[k])
                max_device.append({i: {max_key: temp[max_key]}})
            # ========================= no of contacts in week =============================
            no_of_contacts_week = 0
            for i in pair_count.values():
                for j in i.values():
                    if not j == 0:
                        no_of_contacts_week += 1
            # ============================== max contact duration with user week ===================
            max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
            temp_max = {}
            for i in max_duration:
                temp_max.update({list(i.keys())[0]: list(i.values())[0]})
            max_user, max_duration = max(temp_max.items(), key=lambda k: k[1])
            max_user = [a for a, v in users_device_id.items() if v == max_user][0]

            # max_duration = max([[j['duration'] for j in i] for i in max_pair.values()][0])
            max_duration_week = ms_to_time(max_duration) + '  ' + max_user
            # ============================= contact history week  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None


                pair_count.update({i: temp})

            contact_history_week = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history_week.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})

            hours_list = []
            hours_list.append(current_date)
            current_date = dateTOtimestamp(current_date)

            for i in range(24):
                mints_back = current_date + datetime.timedelta(hours=i + 1)
                hours_list.append(mints_back)
            hour_wise_count = []

            for hr in range(len(hours_list) - 1):

                mints_back = hours_list[hr]
                current_date = hours_list[hr + 1]
                # ================================ slice the all events by given date================
                all_events = []
                if not contact_events == []:
                    for event in contact_events:
                        event_time_epoch = event['start']
                        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                        if count == 0:
                            mints_back = dateTOtimestamp(mints_back)
                        if isNowInTimePeriod(mints_back, current_date, event_time):
                            all_events.append(event)
                        count = count + 1
                result = len(all_events)
                hour_wise_count.append(result)

                user_data.update({u: {
                    'Current Device ID': device_id,
                    'Users in Contact Day': no_of_contacts,
                    'Users in Contact Week': no_of_contacts_week,
                    'Max Contact Duration Day': max_duration_day,
                    'Max Contact Duration Week': max_duration_week,
                    'Contact History Day': contact_history,
                    'Contact History Week': contact_history_week,
                    '24_hr_clock': hour_wise_count
                }})

        else:
        # =============================== Final dict append =========================
            user_data.update({u:{
                'Current Device ID': device_id,
                'Users in Contact Day': None,
                'Users in Contact Week': None,
                'Max Contact Duration Day': None,
                'Max Contact Duration Week':None,
                'Contact History Day': None,
                'Contact History Week': None,
                '24_hr_clock': None
            }})
        # break
    return Response(user_data)
'''


@api_view(['GET'])
def User_history_data(request, startTime, endTime, current_date, regions):
    data = default_getall(regions)
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    user_data = {}
    current_date = dateTOtimestamp(current_date)
    for u, user in data.items():
        # count = 0
        # print(u)
        device_id = user['device_id']
        contact_events = user['contact_event']
        if not contact_events == []:
            max_pair = {}
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    max_pair.update({u: sliced_events})
            contact_all = copy.deepcopy(sliced_events)
            if not sliced_events == []:
                max_device = []
                pair_count = {}
                for i, j in max_pair.items():
                    temp = {}
                    for users in users_device_id.values():
                        temp.update({users: 0})
                    for event in j:
                        temp[event['remote']] += 1
                    pair_count.update({i: temp})
                    max_key = max(temp, key=lambda k: temp[k])
                    max_device.append({i: {max_key: temp[max_key]}})
                # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
                # ========================= no of contacts in single day =============================
                no_of_contacts = 0
                for i in pair_count.values():
                    for j in i.values():
                        if not j == 0:
                            no_of_contacts += 1
                # =============================== contact event all ===================================
                contact_event_all = []
                for i in contact_all:
                    remote = i['remote']
                    remote = [a for a, v in users_device_id.items() if v == remote][0]
                    time_st = datetime.datetime.utcfromtimestamp(i['start'] / 1000)
                    time_st = time_st.replace(tzinfo=pytz.utc).astimezone(timezone)
                    time_st = time_st.strftime("%I:%M %p  %D")
                    avgdist = int(i['avgDist']) / 10
                    minDist = int(i['minDist']) / 10
                    avgdist = cm_to_ft(avgdist)
                    minDist = cm_to_ft(minDist)
                    temp_dict = {
                        remote: {'timestamp': time_st, 'duration': ms_to_time(i['duration']),
                                 'avgDist': avgdist, 'minDist': minDist}
                    }
                    contact_event_all.append(temp_dict)
                # ============================== max contact duration with user ===================
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                try:
                    max_dur = max(max_duration, key=lambda x: list(x.values())[0])
                    max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
                    max_user = [a for a, v in users_device_id.items() if v == max_user][0]
                    max_duration_day = ms_to_time(max_duration) + '  ' + max_user
                except:
                    max_duration_day = None
            else:
                no_of_contacts = 0
                max_duration_day = None
                contact_event_all = []
            # ========================================= clock chart =================================================
            hours_list = []
            hours_list.append(current_date)
            for i in range(24):
                mints_back = current_date + datetime.timedelta(hours=i + 1)
                hours_list.append(mints_back)
            hour_wise_count = []
            for hr in range(len(hours_list) - 1):
                mints_back = hours_list[hr]
                current_date_1 = hours_list[hr + 1]
                # mints_back = dateTOtimestamp(mints_back)
                # ================================ slice the all events by given date================
                all_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod(mints_back, current_date_1, event_time):
                        all_events.append(event)
                    # count = count + 1
                result = len(all_events)
                hour_wise_count.append(result)
            # ============================= contact history single day  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None
                pair_count.update({i: temp})
            contact_history = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
            # =========================================================================================
            # ================================== week wise ============================================
            max_pair = {}
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})
            if not sliced_events == []:
                max_device = []
                pair_count = {}
                for i, j in max_pair.items():
                    temp = {}
                    for users in users_device_id.values():
                        temp.update({users: 0})
                    for event in j:
                        temp[event['remote']] += 1
                    pair_count.update({i: temp})
                    max_key = max(temp, key=lambda k: temp[k])
                    max_device.append({i: {max_key: temp[max_key]}})
                # ========================= no of contacts in week =============================
                no_of_contacts_week = 0
                for i in pair_count.values():
                    for j in i.values():
                        if not j == 0:
                            no_of_contacts_week += 1
                # ============================== max contact duration with user week ===================
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                temp_max = {}
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                try:
                    max_dur = max(max_duration, key=lambda x: list(x.values())[0])
                    max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
                    max_user = [a for a, v in users_device_id.items() if v == max_user][0]
                    max_duration_week = ms_to_time(max_duration) + '  ' + max_user
                except:
                    max_duration_week = None
            else:
                no_of_contacts_week = 0
                max_duration_week = None
            # ============================= contact history week  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None
                pair_count.update({i: temp})
            contact_history_week = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history_week.update(
                            {second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
            # =============================== Final dict append =========================
            user_data.update({u: {
                'Current Device ID': device_id,
                'Device History': user['history_of_ids'],
                'Users in Contact Day': no_of_contacts,
                'Users in Contact Week': no_of_contacts_week,
                'Max Contact Duration Day': max_duration_day,
                'Max Contact Duration Week': max_duration_week,
                'Contact History Day': contact_history,
                'Contact History Week': contact_history_week,
                '24_hr_clock': hour_wise_count,
                'contact_event_all': contact_event_all
            }})
        else:
            user_data.update({u: {
                'Current Device ID': device_id,
                'Device History': user['history_of_ids'],
                'Users in Contact Day': 0,
                'Users in Contact Week': 0,
                'Max Contact Duration Day': None,
                'Max Contact Duration Week': None,
                'Contact History Day': None,
                'Contact History Week': None,
                '24_hr_clock': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                'contact_event_all': []
            }})
    return Response(user_data)


@api_view(['GET'])
def team_history(request, startTime, endTime, current_date, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)
        # ======================== no of users in contact day ==================
        count_dict = {}
        for i, j in result_dict.items():
            for k, v in j.items():
                if not v == 0:
                    if k in count_dict:
                        count_dict[k] += v
                    else:
                        count_dict.update({k: v})
        no_of_users_in_contact = len(count_dict)
        # ========================= no of contacts in a day =============================
        no_of_contacts = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts += j
        # ========================== Users with maximum contact in a day ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))
        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a day ======================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_day = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_day = {' ': 0}
        except:
            most_contact_user_pair_day = {' ': 0}

        # ============================================================================================
        # ================================== week wise =====2444=========================================
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)

        # ========================= no of contacts in a week =============================
        no_of_contacts_week = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts_week += j
        # ========================== Users with maximum contact in a week ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))

        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact_week = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a week =====================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_week = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_week = {' ': 0}
        except:
            most_contact_user_pair_week = {' ': 0}
        # =============================== Final dict append =========================
        team_dict.update({t: {
            'Users in Contact': no_of_users_in_contact,
            'Contacts in a Day': no_of_contacts,
            'Contacts in a Week': no_of_contacts_week,
            'top 5 users in contact Day': top_five_users_max_no_contact,
            'top 5 users in contact Week': top_five_users_max_no_contact_week,
            'Most contact user pair Day': most_contact_user_pair_day,
            'Most contact user pair Week': most_contact_user_pair_week
        }})


    return Response(team_dict)


IST = pytz.timezone('Asia/Calcutta')


def dateTOtimestamp(startdate):
    t = startdate.split('-')
    event_time = datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
    startTime = event_time.replace(tzinfo=timezone).astimezone(timezone)

    return startTime


def ref_User_history_data(startTime, endTime, current_date, regions):
    data = default_getall(regions)
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})


    user_data = {}
    current_date = dateTOtimestamp(current_date)
    for u, user in data.items():
        # count = 0
        # print(u)
        device_id = user['device_id']
        contact_events = user['contact_event']
        if not contact_events == []:
            max_pair = {}
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    max_pair.update({u: sliced_events})
            contact_all = copy.deepcopy(sliced_events)
            if not sliced_events == []:
                max_device = []
                pair_count = {}
                for i, j in max_pair.items():
                    temp = {}
                    for users in users_device_id.values():
                        temp.update({users: 0})
                    for event in j:
                        temp[event['remote']] += 1
                    pair_count.update({i: temp})
                    max_key = max(temp, key=lambda k: temp[k])
                    max_device.append({i: {max_key: temp[max_key]}})
                # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
                # ========================= no of contacts in single day =============================
                no_of_contacts = 0
                for i in pair_count.values():
                    for j in i.values():
                        if not j == 0:
                            no_of_contacts += 1
                # =============================== contact event all ===================================
                contact_event_all = []
                for i in contact_all:
                    remote = i['remote']
                    remote = [a for a, v in users_device_id.items() if v == remote][0]
                    time_st = datetime.datetime.utcfromtimestamp(i['start'] / 1000)
                    time_st = time_st.replace(tzinfo=pytz.utc).astimezone(timezone)
                    time_st = time_st.strftime("%I:%M %p  %D")
                    avgdist = int(i['avgDist']) / 10
                    minDist = int(i['minDist']) / 10
                    avgdist = cm_to_ft(avgdist)
                    minDist = cm_to_ft(minDist)
                    temp_dict = {
                        remote: {'timestamp': time_st, 'duration': ms_to_time(i['duration']),
                                 'avgDist': avgdist, 'minDist': minDist}
                    }
                    contact_event_all.append(temp_dict)
                # ============================== max contact duration with user ===================
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                try:
                    max_dur = max(max_duration, key=lambda x: list(x.values())[0])
                    max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
                    max_user = [a for a, v in users_device_id.items() if v == max_user][0]
                    max_duration_day = ms_to_time(max_duration) + '  ' + max_user
                except:
                    max_duration_day = None
            else:
                no_of_contacts = 0
                max_duration_day = None
                contact_event_all = []
            # ========================================= clock chart =================================================
            hours_list = []
            hours_list.append(current_date)
            for i in range(24):
                mints_back = current_date + datetime.timedelta(hours=i + 1)
                hours_list.append(mints_back)
            hour_wise_count = []
            for hr in range(len(hours_list) - 1):
                mints_back = hours_list[hr]
                current_date_1 = hours_list[hr + 1]
                # mints_back = dateTOtimestamp(mints_back)
                # ================================ slice the all events by given date================
                all_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod(mints_back, current_date_1, event_time):
                        all_events.append(event)
                    # count = count + 1
                result = len(all_events)
                hour_wise_count.append(result)
            # ============================= contact history single day  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None
                pair_count.update({i: temp})
            contact_history = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history.update({second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
            # =========================================================================================
            # ================================== week wise ============================================
            max_pair = {}
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})
            if not sliced_events == []:
                max_device = []
                pair_count = {}
                for i, j in max_pair.items():
                    temp = {}
                    for users in users_device_id.values():
                        temp.update({users: 0})
                    for event in j:
                        temp[event['remote']] += 1
                    pair_count.update({i: temp})
                    max_key = max(temp, key=lambda k: temp[k])
                    max_device.append({i: {max_key: temp[max_key]}})
                # ========================= no of contacts in week =============================
                no_of_contacts_week = 0
                for i in pair_count.values():
                    for j in i.values():
                        if not j == 0:
                            no_of_contacts_week += 1
                # ============================== max contact duration with user week ===================
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                temp_max = {}
                max_duration = [[{j['remote']: j['duration']} for j in i] for i in max_pair.values()][0]
                try:
                    max_dur = max(max_duration, key=lambda x: list(x.values())[0])
                    max_user, max_duration = list(max_dur.keys())[0], list(max_dur.values())[0]
                    max_user = [a for a, v in users_device_id.items() if v == max_user][0]
                    max_duration_week = ms_to_time(max_duration) + '  ' + max_user
                except:
                    max_duration_week = None
            else:
                no_of_contacts_week = 0
                max_duration_week = None
            # ============================= contact history week  ==========================
            pair_count = {}
            for i, j in max_pair.items():
                temp = {}
                for users in users_device_id.values():
                    temp.update({users: {'count': 0, 'max_duration': []}})
                for event in j:
                    temp[event['remote']]['count'] += 1
                    temp[event['remote']]['max_duration'].append(event['duration'])
                for t, tt in temp.items():
                    try:
                        max_dur = max(tt['max_duration'])
                        temp[t]['max_duration'] = ms_to_time(max_dur)
                    except:
                        temp[t]['max_duration'] = None
                pair_count.update({i: temp})
            contact_history_week = {}
            for i in pair_count.values():
                for j, k in i.items():
                    if not k['count'] == 0:
                        second_emp = [a for a, v in users_device_id.items() if v == j][0]
                        contact_history_week.update(
                            {second_emp: {'count': k['count'], 'max_duration': k['max_duration']}})
            # =============================== Final dict append =========================
            user_data.update({u: {
                'Current Device ID': device_id,
                'Device History': user['history_of_ids'],
                'Users in Contact Day': no_of_contacts,
                'Users in Contact Week': no_of_contacts_week,
                'Max Contact Duration Day': max_duration_day,
                'Max Contact Duration Week': max_duration_week,
                'Contact History Day': contact_history,
                'Contact History Week': contact_history_week,
                '24_hr_clock': hour_wise_count,
                'contact_event_all': contact_event_all
            }})
        else:
            user_data.update({u: {
                'Current Device ID': device_id,
                'Device History': user['history_of_ids'],
                'Users in Contact Day': 0,
                'Users in Contact Week': 0,
                'Max Contact Duration Day': None,
                'Max Contact Duration Week': None,
                'Contact History Day': None,
                'Contact History Week': None,
                '24_hr_clock': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                'contact_event_all': []
            }})

    return user_data


def team_history_ref(startTime, endTime, current_date, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)
        # ======================== no of users in contact day ==================
        count_dict = {}
        for i, j in result_dict.items():
            for k, v in j.items():
                if not v == 0:
                    if k in count_dict:
                        count_dict[k] += v
                    else:
                        count_dict.update({k: v})
        no_of_users_in_contact = len(count_dict)
        # ========================= no of contacts in a day =============================
        no_of_contacts = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts += j
        # ========================== Users with maximum contact in a day ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))
        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a day ======================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_day = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_day = {' ':0}
        except:
            most_contact_user_pair_day = {' ':0}

        # ============================================================================================
        # ================================== week wise ==============================================
        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        # ===================== matching back to the user id =========================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})
        # =================== removing the own users count =====================
        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)

        # ========================= no of contacts in a week =============================
        no_of_contacts_week = 0
        for i in pair_count.values():
            for j in i.values():
                no_of_contacts_week += j
        # ========================== Users with maximum contact in a week ===================
        over_al_cont = {}
        for i, j in final_dict.items():
            temp_count = 0
            for v in j.values():
                temp_count += v
            over_al_cont.update({i: temp_count})
        over_al_cont = dict(sorted(over_al_cont.items(), key=lambda x: x[1], reverse=True))

        over_al_count_dup = copy.deepcopy(over_al_cont)
        [over_al_count_dup.pop(i) for i, j in over_al_cont.items() if j == 0]
        top_five_users_max_no_contact_week = dict(list(over_al_count_dup.items())[:5])
        # ============================ Most contact user pair in a week =====================
        most_contact_user_pair = max_device[0]

        final_dict_1 = {}
        for i, j in result_dict.items():
            try:
                max_cont = max(j.items(), key=lambda x: x[1])
            except:
                pass
            user_2 = max_cont[0]
            count = max_cont[1]
            if not count == 0:
                a = [i, user_2]
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in final_dict_1:
                        if final_dict_1[a] < count:
                            final_dict_1.update({a: count})
                    else:
                        final_dict_1.update({a: count})

        try:
            max_cont = max(final_dict_1.items(), key=lambda x: x[1])

            most_contact_user_pair_week = dict([max_cont])

            if max_cont[1] == 0:
                most_contact_user_pair_week = {' ':0}
        except:
            most_contact_user_pair_week = {' ':0}
        # =============================== Final dict append =========================
        team_dict.update({t: {
            'Users in Contact': no_of_users_in_contact,
            'Contacts in a Day': no_of_contacts,
            'Contacts in a Week': no_of_contacts_week,
            'top 5 users in contact Day': top_five_users_max_no_contact,
            'top 5 users in contact Week': top_five_users_max_no_contact_week,
            'Most contact user pair Day': most_contact_user_pair_day,
            'Most contact user pair Week': most_contact_user_pair_week
        }})

    return team_dict


report_path = '/var/www/html/files.com/public_html/coolpad_report/'
public_report = 'http://106.51.3.224:6660/coolpad_report/'

import pandas as pd


@api_view(['GET'])
def report(request, username, filetype, startTime, endTime, current_date, regions):
    data = ref_User_history_data(startTime, endTime, current_date, regions)
    d = data[username]
    d['No of Users in Contact per Day'] = d.pop('Users in Contact Day')
    d['No of Users in Contact per Week'] = d.pop('Users in Contact Week')
    d['Maximum Contact Duration per Day'] = d.pop('Max Contact Duration Day')
    d['Maximum Contact Duration per Week'] = d.pop('Max Contact Duration Week')
    df = pd.DataFrame(d.items(), columns=[None, username])
    change_permissions_recursive(report_path, 0o777)
    if filetype == 'csv':
        df.to_csv(report_path + username + 'History.csv', index=False)
    else:
        df.to_excel(report_path + username + 'History.xlsx', index=False)
    change_permissions_recursive(report_path, 0o777)
    return Response({'status': 'success', 'path': public_report + username + 'History'})


def reportpdf(username, startTime, endTime, current_date, regions):
    data = ref_User_history_data(startTime, endTime, current_date, regions)
    d = data[username]
    d['No of Users in Contact per Day'] = d.pop('Users in Contact Day')
    d['No of Users in Contact per Week'] = d.pop('Users in Contact Week')
    d['Maximum Contact Duration per Day'] = d.pop('Max Contact Duration Day')
    d['Maximum Contact Duration per Week'] = d.pop('Max Contact Duration Week')
    change_permissions_recursive(report_path, 0o777)
    df = pd.DataFrame(d.items(), columns=[None, username])
    df.to_csv(report_path + username + 'History.csv', index=False)
    return report_path + username + 'History.csv'


import csv, os


@api_view(['GET'])
def pdf_gen(request, user, startTime, endTime, current_date, regions):
    # Introduction text
    username = user
    csv_file = reportpdf(username, startTime, endTime, current_date, regions)

    line1 = 'Report for ' + user
    current_date = dateTOtimestamp(current_date)
    line2 = 'Date: ' + current_date.strftime("%B %d %Y")
    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])
    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)
    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'
    # File that must be written to report
    change_permissions_recursive(report_path, 0o777)
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)
    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista][1:]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch
    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + username + 'History.pdf', pagesize=A4, rightMargin=30, leftMargin=30,
                                    topMargin=40, bottomMargin=28)
    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)
    archivo_pdf.build(elements)
    change_permissions_recursive(report_path, 0o777)

    return Response({'status': 'success', 'path': public_report + username + 'History'})


@api_view(['GET'])
def team_report(request, username, filetype, startTime, endTime, current_date, regions):
    team = username
    team_history1 = team_history_ref(startTime, endTime, current_date, regions)
    d = team_history1[username]
    d['No of Users in Contact per Day'] = d.pop('Users in Contact')
    d['No of Contact in a Day'] = d.pop('Contacts in a Day')
    d['No of Contact in a Week'] = d.pop('Contacts in a Week')
    d['Top 5 Users in contact in a Day'] = d.pop('top 5 users in contact Day')
    d['Top 5 Users in contact in a Week'] = d.pop('top 5 users in contact Week')
    d['Most contact user pair in a Day'] = d.pop('Most contact user pair Day')
    d['Most contact user pair in a Week'] = d.pop('Most contact user pair Week')
    if d['Most contact user pair in a Day'] == {' ': 0}:
        d['Most contact user pair in a Day'] = None
    if d['Most contact user pair in a Week'] == {' ': 0}:
        d['Most contact user pair in a Week'] = None


    df = pd.DataFrame(d.items(), columns=[None, team])
    change_permissions_recursive(report_path, 0o777)

    if filetype == 'csv':
        # df = df.T
        df.to_csv(report_path + team + '-Team History.csv', index=False)
    else:
        df.to_excel(report_path + team + '-Team History.xlsx', index=False)
    change_permissions_recursive(report_path, 0o777)

    return Response({'status': 'success', 'path': public_report + team + '-Team History'})


def team_report_ref(username, startTime, endTime, current_date, regions):
    team = username
    team_history1 = team_history_ref(startTime, endTime, current_date, regions)
    d = team_history1[username]
    d['No of Users in Contact per Day'] = d.pop('Users in Contact')
    d['No of Contact in a Day'] = d.pop('Contacts in a Day')
    d['No of Contact in a Week'] = d.pop('Contacts in a Week')
    d['Top 5 Users in contact in a Day'] = d.pop('top 5 users in contact Day')
    d['Top 5 Users in contact in a Week'] = d.pop('top 5 users in contact Week')
    d['Most contact user pair in a Day'] = d.pop('Most contact user pair Day')
    d['Most contact user pair in a Week'] = d.pop('Most contact user pair Week')
    if d['Most contact user pair in a Day'] == {' ': 0}:
        d['Most contact user pair in a Day'] = None
    if d['Most contact user pair in a Week'] == {' ': 0}:
        d['Most contact user pair in a Week'] = None

    change_permissions_recursive(report_path, 0o777)

    df = pd.DataFrame(d.items(), columns=[None, team])

    df.to_csv(report_path + team + '-Team History.csv', index=False)
    return report_path + team + '-Team History.csv'


@api_view(['GET'])
def pdf_gen_team(request, team, startTime, endTime, current_date, regions):
    # Introduction text
    csv_file = team_report_ref(team, startTime, endTime, current_date, regions)
    current_date = dateTOtimestamp(current_date)

    line1 = 'Report for ' + team
    line2 = 'Date: ' + current_date.strftime("%B %d %Y")
    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])
    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)
    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'
    # File that must be written to report
    change_permissions_recursive(report_path, 0o777)
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)
    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista][1:]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch
    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + team + '-Team History.pdf', pagesize=A4, rightMargin=30,
                                    leftMargin=30, topMargin=40, bottomMargin=28)
    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)
    archivo_pdf.build(elements)
    change_permissions_recursive(report_path, 0o777)

    return Response({'status': 'success', 'path': public_report + team + '-Team History'})


# def get_top_five_tree(data, user, startTime, endTime, users_device_id):
#     max_pair = {}
#     sliced_events = []
#     primery_target = user
#     contact_events = data[primery_target]['contact_event']
#     if not contact_events == []:
#         for event in contact_events:
#             event_time_epoch = event['start']
#             event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
#             event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
#             if isNowInTimePeriod(startTime, endTime, event_time):
#                 sliced_events.append(event)
#             max_pair.update({primery_target: sliced_events})
#
#     pair_count = {}
#     for i, j in max_pair.items():
#         temp = {}
#         for users in users_device_id.values():
#             emp_name = [a for a, v in users_device_id.items() if v == users][0]
#             temp.update({emp_name: {'count': 0, 'max_duration': [], 'Date': []}})
#         for event in j:
#             remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
#             temp[remote]['count'] += 1
#             temp[remote]['max_duration'].append(event['duration'])
#             temp[remote]['Date'].append(event['start'])
#         for t, tt in temp.items():
#             try:
#                 max_dur = max(tt['max_duration'])
#                 temp[t]['max_duration'] = ms_to_time(max_dur)
#             except:
#                 temp[t]['max_duration'] = None
#         for t, tt in temp.items():
#             try:
#                 start_date = min(tt['Date'])
#
#                 event_time = datetime.datetime.utcfromtimestamp(int(start_date) / 1000)
#                 temp[t]['Date'] = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
#             except:
#                 temp[t]['Date'] = None
#         temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))
#         pair_count.update({i: temp})
#
#     # top_five = dict(list(list(pair_count.values())[0].items())[:5])
#     top_five = dict(list(list(pair_count.values())[0].items()))
#
#     # top_five = pair_count
#     result_dict = copy.deepcopy(top_five)
#     for i, j in top_five.items():
#         if j['count'] == 0:
#             result_dict.pop(i)
#
#     for i, j in result_dict.items():
#         team = data[i]['team']
#         result_dict[i].update({'team': team})
#
#     return {primery_target: result_dict}



def get_top_five_tree(data, user, startTime, endTime, users_device_id):
    max_pair = {}

    primery_target = user
    
    for i, j in data.items():

        sliced_events = []
        contact_events = j['contact_event']
        # if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
        max_pair.update({i: sliced_events})
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({emp_name: {'count': 0, 'max_duration': [], 'Date': []}})
        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1
            temp[remote]['max_duration'].append(event['duration'])
            temp[remote]['Date'].append(event['start'])

        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None

        for t, tt in temp.items():
            try:
                start_date = min(tt['Date'])

                event_time = datetime.datetime.utcfromtimestamp(int(start_date) / 1000)
                temp[t]['Date'] = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            except:
                temp[t]['Date'] = None

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count.update({i: temp})

    target_dict = {primery_target: pair_count[primery_target]}

    f = copy.deepcopy(target_dict)

    for i, j in f[primery_target].items():
        temp_user_count = j['count']
        if not i == primery_target:
            user_count = pair_count[i][primery_target]['count']
            if user_count > temp_user_count:
                target_dict[primery_target][i]['count'] = user_count
                target_dict[primery_target][i]['max_duration'] = pair_count[i][primery_target]['max_duration']
                target_dict[primery_target][i]['Date'] = pair_count[i][primery_target]['Date']


    top_five = dict(list(list(target_dict.values())[0].items()))
    result_dict = copy.deepcopy(top_five)
    for i, j in top_five.items():
        if j['count'] == 0:
            result_dict.pop(i)

    for i, j in result_dict.items():
        team = data[i]['team']
        result_dict[i].update({'team': team})

    return {primery_target: result_dict}



@api_view(['GET'])
def contact_trace(request, user, startTime, endTime, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    final_list = []
    final_list.append({"searched_emp_team": data[user]['team']})
    top_five = get_top_five_tree(data, user, startTime, endTime, users_device_id)
    final_list.append(top_five)

    # tree_dict = copy.deepcopy(top_five)
    for i in top_five.values():
        temp = []
        for j, k in i.items():
            user = j
            top_five = get_top_five_tree(data, user, startTime, endTime, users_device_id)
            temp.append(top_five)
    final_list.append(temp)

    return Response(final_list)


def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root, d) for d in dirs]:
            os.chmod(dir, mode)
    for file in [os.path.join(root, f) for f in files]:
        os.chmod(file, mode)


@api_view(['GET'])
def clk_chart_weekly(request, current_date, regions):
    data = default_getall(regions)
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 12, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    hours_list = []
    hours_list.append(current_date)
    for i in range(5):
        mints_back = current_date + datetime.timedelta(days=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod(mints_back, current_date_1, event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})
        # ================================Duplicate removal  ============================
        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)
        result = len(all_events)
        hour_wise_count.append(result)
    return Response(hour_wise_count)


@api_view(['GET'])
def outliers(request, startTime, endTime, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    top_pairs = dict(list(top_pairs.items())[:2])
    result_dict = {}
    for i, j in top_pairs.items():
        e1, e2 = i.split(' - ')
        result_dict.update({e1: j})
        result_dict.update({e2: j})
    return Response(result_dict)


@api_view(['POST'])
@authentication_classes([])
def deleteEmps(request, deviceID):
    userdata = UserProfile.objects.filter(device_id=deviceID)
    for k in userdata:
        k.is_Deleted = 'true'
        k.save()
    return Response({"deleted": "true"})


def slice_string(a):
    res = a.split("__")
    return res[0], res[1], res[2], res[3]


@api_view(['GET'])
@authentication_classes([])
def getTeams(request, regions):
    alldata = default_getall(regions)
    teams = []
    for i in alldata:
        print(alldata[i]['team'])
        teams.append(alldata[i]['team'])
        teams = list(dict.fromkeys(teams))

    return Response({'teams': teams})


@api_view(['GET'])
@authentication_classes([])
def missedDevice(request, regions):
    alldata = default_getall(regions)
    resp = {}
    for i in alldata:
        ht = alldata[i]['recent_heartbeat_event']
        try:
            a = str(ht['shutdown'])
            if a == 'False':
                event_time = datetime.datetime.utcfromtimestamp(int(ht['tstamp']) / 1000)
                dt_object = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                now = datetime.datetime.now(timezone)
                diff = dt_object - now
                minutes = divmod(diff.total_seconds(), 60)
                hours = int(abs(minutes[0])) // 3600
                min = int(abs(minutes[0])) // 60
                if min > 120:
                    resp[str(i)] = {
                        'emp': i,
                        'date': datetime.datetime.now(timezone),
                        'deviceID': ht['local'],
                        'duration': hours
                    }
        except:
            print('')
    return Response(resp)


@api_view(['GET'])
@authentication_classes([])
def notifyDevice(request, regions):
    alldata = default_getall(regions)
    resp = {}
    for i in alldata:
        ht = alldata[i]['recent_heartbeat_event']
        try:
            a = str(ht['shutdown'])
            if a == 'False':
                event_time = datetime.datetime.utcfromtimestamp(int(ht['tstamp']) / 1000)
                dt_object = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                now = datetime.datetime.now(timezone)
                diff = dt_object - now
                minutes = divmod(diff.total_seconds(), 60)
                hours = int(abs(minutes[0])) // 3600
                min = int(abs(minutes[0])) // 60
                if min > 60:
                    resp[str(i)] = {
                        'emp': i,
                        'date': datetime.datetime.now(timezone),
                        'deviceID': ht['local'],
                        'duration': hours
                    }
        except:
            print('')
    return Response(resp)


'''

datetime.now(timezone)

'''


@api_view(['GET'])
def outliers_report(request, filetype, startTime, endTime, regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    all_events = []
    max_pair = {}

    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(dateTOtimestamp(startTime), dateTOtimestamp(endTime), event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    top_pairs = dict(list(top_pairs.items())[:2])
    result_dict = {}
    for i, j in top_pairs.items():
        e1, e2 = i.split(' - ')
        result_dict.update({e1: j})
        result_dict.update({e2: j})
    outlier_users = result_dict
    df = pd.DataFrame(outlier_users.items(), columns=['Employees Pair', 'Count'])
    change_permissions_recursive(report_path, 0o777)
    if filetype == 'csv':
        df.to_csv(report_path + startTime + 'Outliers.csv', index=False)
    else:
        df.to_excel(report_path + startTime + 'Outliers.xlsx', index=False)
    change_permissions_recursive(report_path, 0o777)
    return Response({'status': 'success', 'path': public_report + startTime + 'Outliers'})


# ================================= gencsv, excel and  pdf ========================================================


from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet


@api_view(('GET',))
def pdf_gen_outlier(request, startTime, endTime, current_date, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:

            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(dateTOtimestamp(startTime), dateTOtimestamp(endTime), event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })
    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    top_pairs = dict(list(top_pairs.items())[:2])
    result_dict = {}
    for i, j in top_pairs.items():
        e1, e2 = i.split(' - ')
        result_dict.update({e1: j})
        result_dict.update({e2: j})
    outlier_users = result_dict
    df = pd.DataFrame(outlier_users.items(), columns=['Employees Pair', 'Count'])
    change_permissions_recursive(report_path, 0o777)
    df.to_csv(report_path + startTime + 'Outliers.csv', index=False)

    csv_file = report_path + startTime + 'Outliers.csv'
    # Introduction text
    line1 = 'Outliers Report'
    line2 = 'Date: ' + current_date

    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    # File that must be written to report
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)

    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + current_date + 'Outliers.pdf', pagesize=A4, rightMargin=30,
                                    leftMargin=30, topMargin=40,
                                    bottomMargin=28)

    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)

    archivo_pdf.build(elements)
    change_permissions_recursive(report_path, 0o777)
    return Response({'status': 'success', 'path': public_report + current_date + 'Outliers'})


@api_view(['GET'])
def daily_report(request, current_date, regions):
    combined_daily_tracker=daily_tracker_get_ref(current_date,regions)
    print(combined_daily_tracker ,"HETYYYY")
    top_pairs = combined_daily_tracker["top_pairs_maxmium_contact"]
    pairs_history = combined_daily_tracker["contact_History_all"]
    total_no_contacts = combined_daily_tracker["total_no_of_contacts"]
    current_time = datetime.datetime.now(timezone)
    line1 = 'Daily Tracker Report'
    line2 = 'Date: ' + current_date
    line3 = 'Total no of contacts: ' + str(total_no_contacts)
    line4 = 'Top Contact History (Top 5)'
    line5 = 'Contact History for All Users'

    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    lista = []
    lista.append(['Employees Pair', 'Count'])
    for i, j in top_pairs.items():
        lista.append([i, str(j)])

    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    listb = []
    listb.append(['Employees Pair', 'Count'])
    for i, j in pairs_history.items():
        listb.append([i, str(j)])
    data3 = [[Paragraph(cell, s) for cell in row] for row in listb]
    t2 = Table(data3)
    t2.setStyle(table_style)

    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + current_date + '_daily_report.pdf', pagesize=A4, rightMargin=30,
                                    leftMargin=30, topMargin=40,
                                    bottomMargin=28)

    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line3, styleNormal))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line4, styleNormal))
    elements.append(t)
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line5, styleNormal))
    elements.append(t2)

    archivo_pdf.build(elements)
    return Response({'status': 'success', 'path': public_report + current_date + '_daily_report'})


def top_pairs_maxmium_contact_Report(data, current_date, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                # event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    return top_pairs


def pairs_maxmium_contact_Report(data, current_date, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    return no_dublicat_dict


def total_no_of_contacts_Report(data, current_date):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================Duplicate removal  ============================

    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)

    result = len(all_events)

    return result


def freqency_matrix_Report(data, current_date, users_device_id):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date == str(event_time.date()):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

        final_dict = {}
        for i in max_device:
            first_emp = list(i.keys())[0]
            second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
            })

        # ================================Duplicate removal  ============================
        no_dublicat_dict = {}
        for i, j in final_dict.items():
            if not j == 0:
                a = i.split(' - ')
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in no_dublicat_dict:
                        if no_dublicat_dict[a] < j:
                            no_dublicat_dict.update({a: j})
                    else:
                        no_dublicat_dict.update({a: j})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        for i, j in no_dublicat_dict.items():
            a1, a2 = i.split(' - ')
            try:
                final_dict[a1][a2] = j
                final_dict[a2][a1] = j
            except:
                pass

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict.update({t: result_dict})

    return team_dict


def last_5_mints_Report(data, users_device_id, mints_back):
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod_R(mints_back, current_date, event_time):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})

    live_list = []
    for i in all_events:
        temp = {}
        local = [k for k, v in users_device_id.items() if v == i['local']][0]
        remote = [k for k, v in users_device_id.items() if v == i['remote']][0]
        event_time = datetime.datetime.utcfromtimestamp(i['start'] / 1000)

        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        event_time = event_time.strftime("%I:%M:%S %p")
        avgdist = str(int(i['avgDist'] / 10)) + ' cm'
        minDist = str(int(i['minDist'] / 10)) + ' cm'
        temp.update({'local': local, 'remote': remote, 'start': event_time, 'duration': ms_to_time(i['duration']),
                     'avgDist': avgdist, 'minDist': minDist})
        live_list.append(temp)

    return live_list


# ========================================= clock chart =================================================
def clk_chart_Report(data, current_date):
    current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 00, 00)

    hours_list = []
    hours_list.append(current_date)
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)

    hour_wise_count = []
    for hr in range(len(hours_list) - 1):

        mints_back = hours_list[hr]
        current_date = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)

                    event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                    if isNowInTimePeriod_R(mints_back, current_date, event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})

        # ================================Duplicate removal  ============================

        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = local_time - remote_time
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)

        result = len(all_events)
        hour_wise_count.append(result)

    return hour_wise_count


def isNowInTimePeriod_R(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


@api_view(['GET'])
def weekly_report(request, startTime, endTime, current_date, regions):
    weekly_tracker_dict= weekly_tracker_get_ref(startTime, endTime, regions)
    total_no_contacts = weekly_tracker_dict["total_no_of_contacts_weekly"]
    top_pairs = weekly_tracker_dict["top_pairs_maxmium_contact_week"]
    pairs_history = weekly_tracker_dict["pairs_history_weekly"]
    line1 = 'Weekly Tracker Report'
    line2 = 'Start Date: ' + startTime
    line2_1 = 'End Date: ' + endTime
    line3 = 'Total no of contacts: ' + str(total_no_contacts)
    line4 = 'Top Contact History (Top 5)'
    line5 = 'Contact History for All Users'
    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    lista = []
    lista.append(['Employees Pair', 'Count'])
    for i, j in top_pairs.items():
        lista.append([i, str(j)])

    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    listb = []
    listb.append(['Employees Pair', 'Count'])
    for i, j in pairs_history.items():
        listb.append([i, str(j)])
    data3 = [[Paragraph(cell, s) for cell in row] for row in listb]
    t2 = Table(data3)
    t2.setStyle(table_style)

    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + current_date + '_weekly_report.pdf', pagesize=A4, rightMargin=30,
                                    leftMargin=30, topMargin=40,
                                    bottomMargin=28)

    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Paragraph(line2_1, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line3, styleNormal))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line4, styleNormal))
    elements.append(t)
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line5, styleNormal))
    elements.append(t2)

    archivo_pdf.build(elements)
    return Response({'status': 'success', 'path': public_report + current_date + '_weekly_report'})


def total_no_of_contacts_Weekly(data, startTime, endTime):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================Duplicate removal  ============================

    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)

    result = len(all_events)

    return result


def top_pairs_maxmium_contact_Weekly(data, startTime, endTime, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    return top_pairs

# def top_pairs_maxmium_contact(data, startTime, endTime, users_device_id):
#     # ================================ slice the all events by given date================
#     all_events = []
#     max_pair = {}
#     for i, j in data.items():
#         contact_events = j['contact_event']
#         if not contact_events == []:
#             sliced_events = []
#             for event in contact_events:
#                 event_time_epoch = event['start']
#                 event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
#                 event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
#                 if isNowInTimePeriod(startTime, endTime, event_time):
#                     sliced_events.append(event)
#                     all_events.append(event)
#             max_pair.update({i: sliced_events})
#
#     # ================================ Find the pair counts ================
#     max_device = []
#     pair_count = {}
#     for i, j in max_pair.items():
#         temp = {}
#         for users in users_device_id.values():
#             temp.update({users: 0})
#         for event in j:
#             temp[event['remote']] += 1
#         pair_count.update({i: temp})
#
#         max_key = max(temp, key=lambda k: temp[k])
#         max_device.append({i: {max_key: temp[max_key]}})
#
#     max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)
#
#     # ================================ matching back the device id with user id  ================
#     final_dict = {}
#     for i in max_device:
#         first_emp = list(i.keys())[0]
#         second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
#         final_dict.update({
#             first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
#         })
#
#     # ================================Duplicate removal  ============================
#     no_dublicat_dict = {}
#     for i, j in final_dict.items():
#         a = i.split(' - ')
#         a = natsorted(a)
#         a = ' - '.join(a)
#         no_dublicat_dict.update({a: j})
#
#     # ================================ Get the Top Pairs ============================
#     top_pairs_number = 5  # number of top pairs needed
#
#     top_pairs = {}
#     count = 0
#     for i, j in no_dublicat_dict.items():
#         if count == top_pairs_number:
#             break
#         top_pairs.update({i: j})
#         count += 1
#
#     return top_pairs

def pairs_maxmium_contact_Weekly(data, startTime, endTime, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    return no_dublicat_dict


@api_view(['GET'])
def contact_tracing_report(request, user, startTime, endTime, regions):
    line1 = 'Contact Tracing'
    line2 = 'Start Date: ' + startTime
    line2_1 = 'End Date: ' + endTime
    line3 = 'Contacted Person : ' + user
    line4 = 'Direct Contacts of ' + user
    line5 = 'Contact History for All Users'
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    final_list = []
    top_five = get_top_five_tree(data, user, dateTOtimestamp(startTime), dateTOtimestamp(endTime), users_device_id)
    final_list.append(top_five)
    for i in top_five.values():
        temp = []
        for j, k in i.items():
            user = j
            top_five = get_top_five_tree(data, user, dateTOtimestamp(startTime), dateTOtimestamp(endTime),
                                         users_device_id)
            temp.append(top_five)
    final_list.append(temp)
    trace = final_list
    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])
    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'
    lista = []
    lista.append(['Employee', 'Count', 'Maximum Duration', 'Date', 'Team'])
    for i, j in list(trace[0].values())[0].items():
        lista.append([i, str(j['count']), j['max_duration'], j['Date'].strftime("%B %d %Y"), j['team']])
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    elements = []
    archivo_pdf = SimpleDocTemplate(report_path + user + "tracing.pdf", pagesize=A4, rightMargin=30, leftMargin=30,
                                    topMargin=40,
                                    bottomMargin=28)
    im = Image('/home/prime/Documents/coolpad_backend/logo.png', width=230, height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Paragraph(line2_1, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line3, styleNormal))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(Paragraph(line4, styleNormal))
    elements.append(t)
    archivo_pdf.build(elements)
    return Response({'status': 'success', 'path': public_report + user + 'tracing'})


@api_view(['POST'])
def delete_invalid_data(request, regions):
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    valid_device_ids = list(users_device_id.values())
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    resp = {}
    for k in alluser:
        contact_events = json.loads(k.contact_event)
        contact_events_copy = copy.deepcopy(contact_events)
        for i in contact_events:
            remote = i['remote']
            if remote not in valid_device_ids:
                contact_events_copy.remove(i)
        k.contact_event = json.dumps(contact_events_copy)
        k.save()
    return Response({'status': 'success'})


@api_view(["GET"])
def weekly_tracker_get(request, startTime, endTime, regions):
    # def pairs_history_weekly(request, startTime,endTime,regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    current_date = startTime

    users_device_id = {}

    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)

    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        contact_events = [k for n, k in enumerate(contact_events) if k not in contact_events[n + 1:]]
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ===================================== pair count ============================================

    final_dict = {}
    for i, j in pair_count.items():
        first_emp = i
        for k, l in j.items():
            second_emp = [m for m, n in users_device_id.items() if n == k][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: l
            })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    no_dublicat_dict = dict(sorted(no_dublicat_dict.items(), key=lambda x: x[1], reverse=True))

    contact_history_all = copy.deepcopy(no_dublicat_dict)

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    # ================================Duplicate removal  ============================

    duplicate_events = copy.deepcopy(all_events)
    for i in duplicate_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in duplicate_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == remote_temp and remote == local_temp:
                tim_diff = int(local_time) - int(remote_time)
                e1 = datetime.datetime.utcfromtimestamp(int(local_time) / 1000)
                e1 = e1.replace(tzinfo=pytz.utc).astimezone(timezone)

                e2 = datetime.datetime.utcfromtimestamp(int(remote_time) / 1000)
                e2 = e2.replace(tzinfo=pytz.utc).astimezone(timezone)

                g = e1 - e2
                if g.days == -1:
                    g = e2 - e1
                print(g)
                # print(tim_diff)
                if g.seconds < 2:
                    try:
                        duplicate_events.remove(j)
                    except:
                        pass

    result_total_contacts = len(duplicate_events)
    # result_total_contacts = sum(list(no_dublicat_dict_1.values()))

    # ============================================== freqency_matrix_weekly =================================
    # def freqency_matrix_weekly(request, startTime,endTime,regions):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict_matrix = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        for i, j in no_dublicat_dict.items():
            a1, a2 = i.split(' - ')
            try:
                final_dict[a1][a2] = j
                final_dict[a2][a1] = j
            except:
                pass

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict_matrix.update({t: result_dict})
    # return Response(team_dict)
    # ============================ weekly_team_tracker ==========================
    # def weekly_team_tracker(request, startTime,endTime,regions):
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
        # ================================Duplicate removal  ============================
        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)
        result = len(all_events)
        team_dict.update({t: result})
    # return Response(team_dict)
    # ==================================== clk_chart_weekly =============================================
    # def clk_chart_weekly(request,current_date,regions):
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 12, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    hours_list = []
    hours_list.append(current_date)
    for i in range(7):
        mints_back = current_date + datetime.timedelta(days=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []

        for event in duplicate_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_date_1, event_time):
                all_events.append(event)

        result = len(all_events)
        hour_wise_count.append(result)
    # return Response(hour_wise_count)
    weekly_tracker_dict = {
        'pairs_history_weekly': contact_history_all,
        'top_pairs_maxmium_contact_week': top_pairs,
        'total_no_of_contacts_weekly': result_total_contacts,
        'freqency_matrix_weekly': team_dict_matrix,
        'weekly_team_tracker': team_dict,
        'clk_chart_weekly': hour_wise_count
    }

    return Response(weekly_tracker_dict)


@api_view(["GET"])
def daily_tracker_get(request, current_date, regions):
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        contact_events = [k for n, k in enumerate(contact_events) if k not in contact_events[n + 1:]]
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ===================================== pair count ============================================

    final_dict = {}
    for i, j in pair_count.items():
        first_emp = i
        for k, l in j.items():
            second_emp = [m for m, n in users_device_id.items() if n == k][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: l
            })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    no_dublicat_dict = dict(sorted(no_dublicat_dict.items(), key=lambda x: x[1], reverse=True))

    contact_history_all = copy.deepcopy(no_dublicat_dict)

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    # return Response(top_pairs)

    # ================================total number of contacts ============================
    duplicate_events = copy.deepcopy(all_events)
    for i in duplicate_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in duplicate_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == remote_temp and remote == local_temp:
                tim_diff = int(local_time) - int(remote_time)
                e1 = datetime.datetime.utcfromtimestamp(int(local_time) / 1000)
                e1 = e1.replace(tzinfo=pytz.utc).astimezone(timezone)

                e2 = datetime.datetime.utcfromtimestamp(int(remote_time) / 1000)
                e2 = e2.replace(tzinfo=pytz.utc).astimezone(timezone)

                g = e1 - e2
                if g.days == -1:
                    g = e2 - e1
                # print(g)
                # print(tim_diff)
                if g.seconds < 2:
                    try:
                        duplicate_events.remove(j)
                    except:
                        pass

    total_no_of_contacts = len(duplicate_events)
    # return Response(result)
    ##### ============================================== clk chart ========================
    hours_list = [current_date]
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        hourly_events = []
        for event in duplicate_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_date_1, event_time):
                hourly_events.append(event)
        result = len(hourly_events)
        hour_wise_count.append(result)
    # return Response(hour_wise_count)
    combined_daily_tracker = {'top_pairs_maxmium_contact': top_pairs,
                              'total_no_of_contacts': total_no_of_contacts,
                              'clk_chart': hour_wise_count,
                              'contact_History_all': contact_history_all
                              }

    return Response(combined_daily_tracker)


# contact_events = data['E-9']['contact_event']
def merge_events(contact_events):
    remote_users = []
    for i in contact_events:
        if not i['remote'] in remote_users:
            remote_users.append(i['remote'])
    temp_events = []
    for i in remote_users:
        temp = []
        for j in contact_events:
            if j['remote'] == i:
                temp.append(j)
        temp = sorted(temp, key=lambda x: x['start'])
        temp_events.append(temp)
    comb_all = []
    for i in temp_events:
        temp_event = i
        dup_event = copy.deepcopy(i)
        comb_event = []
        temp_comb = [temp_event[0]]
        for e in range(1, len(temp_event)):
            ts_1 = temp_event[e - 1]['start']
            duration_1 = temp_event[e - 1]['duration']
            buffer_seconds = 0
            ts_1 = datetime.datetime.utcfromtimestamp(int(ts_1) / 1000)
            ts_1 = ts_1.replace(tzinfo=pytz.utc).astimezone(timezone)
            exp_time = ts_1 + datetime.timedelta(seconds=int(duration_1 / 1000) + buffer_seconds)
            ts_2 = temp_event[e]['start']
            duration_2 = temp_event[e]['duration']
            ts_2 = datetime.datetime.utcfromtimestamp(int(ts_2) / 1000)
            ts_2 = ts_2.replace(tzinfo=pytz.utc).astimezone(timezone)
            tim_diff = exp_time - ts_2
            if tim_diff.days == -1:
                tim_diff =  ts_2 - exp_time
            if tim_diff.days == 0 and tim_diff.seconds < 8:
                temp_comb.append(temp_event[e])
            else:
                comb_event.append(temp_comb)
                temp_comb = [temp_event[e]]
        comb_event.append(temp_comb)
        comb_all.append(comb_event)
    final_events = []
    for eve in comb_all:
        for i in eve:
            if len(i) == 0:
                final_events.append(i)
            else:
                duration = []
                avg_dist = []
                min_dist = []
                for j in i:
                    duration.append(j['duration'])
                    avg_dist.append(j['avgDist'])
                    min_dist.append(j['minDist'])
                t = i[0]
                t['duration'] = sum(duration)
                t['avgDist'] = int(mean(avg_dist))
                t['minDist'] = int(mean(min_dist))
                final_events.append(t)
    return final_events

def replace_device_id(regions, target_user, new_id,data):
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    old_id = users_device_id[target_user]
    for k in alluser:
        contact_events = json.loads(k.contact_event)
        duplicate_event = []
        if k.user == target_user:
            for eve in contact_events:
                eve['local'] = new_id
                duplicate_event.append(eve)
        else:
            for eve in contact_events:
                remote = eve['remote']
                if remote == old_id:
                    eve['remote'] = new_id
                    duplicate_event.append(eve)
                else:
                    duplicate_event.append(eve)
        k.contact_event = json.dumps(duplicate_event)
        k.save()
    return ({'status': 'success'})

@api_view(["POST"])
@authentication_classes([])
def temp_replace(request,regions):
    '''
    target_user="T-19"
    sliced_group, sliced_loc, sliced_zone, sliced_company = slice_string(regions)
    alluser = UserProfile.objects.filter(
        group=sliced_group,
        locations=sliced_loc,
        zone=sliced_zone,
        company=sliced_company,
    )
    new_id = "90030000783690102205"
    old_id = "90030000783690102013"
    for k in alluser:
        contact_events = json.loads(k.contact_event)
        duplicate_event = []
        if k.user == target_user:
            for eve in contact_events:
                eve['local'] = new_id
                duplicate_event.append(eve)
        else:
            for eve in contact_events:
                remote = eve['remote']
                if remote == old_id:
                    eve['remote'] = new_id
                    duplicate_event.append(eve)
                else:
                    duplicate_event.append(eve)
        k.contact_event = json.dumps(duplicate_event)
        k.save()'''
    return Response({'status': 'success'})

def daily_tracker_get_ref(current_date, regions):
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    data = default_getall(regions)
    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        contact_events = [k for n, k in enumerate(contact_events) if k not in contact_events[n + 1:]]
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ===================================== pair count ============================================

    final_dict = {}
    for i, j in pair_count.items():
        first_emp = i
        for k, l in j.items():
            second_emp = [m for m, n in users_device_id.items() if n == k][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: l
            })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    no_dublicat_dict = dict(sorted(no_dublicat_dict.items(), key=lambda x: x[1], reverse=True))

    contact_history_all = copy.deepcopy(no_dublicat_dict)

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1
    # return Response(top_pairs)

    # ================================total number of contacts ============================
    duplicate_events = copy.deepcopy(all_events)
    for i in duplicate_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in duplicate_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == remote_temp and remote == local_temp:
                tim_diff = int(local_time) - int(remote_time)
                e1 = datetime.datetime.utcfromtimestamp(int(local_time) / 1000)
                e1 = e1.replace(tzinfo=pytz.utc).astimezone(timezone)

                e2 = datetime.datetime.utcfromtimestamp(int(remote_time) / 1000)
                e2 = e2.replace(tzinfo=pytz.utc).astimezone(timezone)

                g = e1 - e2
                if g.days == -1:
                    g = e2 - e1
                # print(g)
                # print(tim_diff)
                if g.seconds < 2:
                    try:
                        duplicate_events.remove(j)
                    except:
                        pass

    total_no_of_contacts = len(duplicate_events)
    # return Response(result)
    ##### ============================================== clk chart ========================
    hours_list = [current_date]
    for i in range(24):
        mints_back = current_date + datetime.timedelta(hours=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        hourly_events = []
        for event in duplicate_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_date_1, event_time):
                hourly_events.append(event)
        result = len(hourly_events)
        hour_wise_count.append(result)
    # return Response(hour_wise_count)
    combined_daily_tracker = {'top_pairs_maxmium_contact': top_pairs,
                              'total_no_of_contacts': total_no_of_contacts,
                              'clk_chart': hour_wise_count,
                              'contact_History_all': contact_history_all
                              }

    return combined_daily_tracker

def weekly_tracker_get_ref(startTime, endTime, regions):
    # def pairs_history_weekly(request, startTime,endTime,regions):
    # ================================ slice the all events by given date================
    data = default_getall(regions)
    current_date = startTime

    users_device_id = {}

    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)

    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        contact_events = [k for n, k in enumerate(contact_events) if k not in contact_events[n + 1:]]
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})
    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})
        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})
    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ===================================== pair count ============================================

    final_dict = {}
    for i, j in pair_count.items():
        first_emp = i
        for k, l in j.items():
            second_emp = [m for m, n in users_device_id.items() if n == k][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: l
            })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        if not j == 0:
            a = i.split(' - ')
            a = natsorted(a)
            if not a[0] == a[1]:
                a = ' - '.join(a)
                if a in no_dublicat_dict:
                    if no_dublicat_dict[a] < j:
                        no_dublicat_dict.update({a: j})
                else:
                    no_dublicat_dict.update({a: j})

    no_dublicat_dict = dict(sorted(no_dublicat_dict.items(), key=lambda x: x[1], reverse=True))

    contact_history_all = copy.deepcopy(no_dublicat_dict)

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed
    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    # ================================Duplicate removal  ============================

    duplicate_events = copy.deepcopy(all_events)
    for i in duplicate_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in duplicate_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == remote_temp and remote == local_temp:
                tim_diff = int(local_time) - int(remote_time)
                e1 = datetime.datetime.utcfromtimestamp(int(local_time) / 1000)
                e1 = e1.replace(tzinfo=pytz.utc).astimezone(timezone)

                e2 = datetime.datetime.utcfromtimestamp(int(remote_time) / 1000)
                e2 = e2.replace(tzinfo=pytz.utc).astimezone(timezone)

                g = e1 - e2
                if g.days == -1:
                    g = e2 - e1
                print(g)
                # print(tim_diff)
                if g.seconds < 2:
                    try:
                        duplicate_events.remove(j)
                    except:
                        pass

    result_total_contacts = len(duplicate_events)
    # result_total_contacts = sum(list(no_dublicat_dict_1.values()))

    # ============================================== freqency_matrix_weekly =================================
    # def freqency_matrix_weekly(request, startTime,endTime,regions):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))
    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})
    team_dict_matrix = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        for i, j in no_dublicat_dict.items():
            a1, a2 = i.split(' - ')
            try:
                final_dict[a1][a2] = j
                final_dict[a2][a1] = j
            except:
                pass

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict_matrix.update({t: result_dict})
    # return Response(team_dict)
    # ============================ weekly_team_tracker ==========================
    # def weekly_team_tracker(request, startTime,endTime,regions):
    team_dict = {}
    for t, team in data_team.items():
        all_events = []
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
        # ================================Duplicate removal  ============================
        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = int(local_time) - int(remote_time)
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)
        result = len(all_events)
        team_dict.update({t: result})
    # return Response(team_dict)
    # ==================================== clk_chart_weekly =============================================
    # def clk_chart_weekly(request,current_date,regions):
    sp = current_date.split('-')
    current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 12, 00, 00)
    current_date = current_date.replace(tzinfo=timezone).astimezone(timezone)
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)
    hours_list = []
    hours_list.append(current_date)
    for i in range(7):
        mints_back = current_date + datetime.timedelta(days=i + 1)
        hours_list.append(mints_back)
    hour_wise_count = []
    for hr in range(len(hours_list) - 1):
        mints_back = hours_list[hr]
        current_date_1 = hours_list[hr + 1]
        # ================================ slice the all events by given date================
        all_events = []

        for event in duplicate_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(mints_back, current_date_1, event_time):
                all_events.append(event)

        result = len(all_events)
        hour_wise_count.append(result)
    # return Response(hour_wise_count)
    weekly_tracker_dict = {
        'pairs_history_weekly': contact_history_all,
        'top_pairs_maxmium_contact_week': top_pairs,
        'total_no_of_contacts_weekly': result_total_contacts,
        'freqency_matrix_weekly': team_dict_matrix,
        'weekly_team_tracker': team_dict,
        'clk_chart_weekly': hour_wise_count
    }

    return weekly_tracker_dict