import copy
import datetime
import json
import random

import dateutil.relativedelta
import pytz
from natsort import natsorted

timezone = pytz.timezone('America/Chicago')

# with open(
#         'data.json') as f:
#     data = json.load(f)

import requests

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
r = requests.get(tyson)
data = r.json()


def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    distance = round(float(distance), 4)
    distance = str(distance) + ' ft'
    # print(distance,"thambi")
    return distance


def dateTOtimestamp(startdate):
    d, t = startdate.split(' ')
    d = d.split('-')
    t = t.split('-')
    event_time = datetime.datetime(int(d[0]), int(d[1]), int(d[2]),
                                   hour=int(t[0]), minute=int(t[1]), second=int(t[2]))

    startTime = timezone.localize(event_time)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + "h " + str(minutes) + 'm ' + str(seconds) + "s"
    return a


def time_to_ms(a):
    # a = '0:4:49'
    a = a.split(' ')
    b = [int(a[0][:-1]) * 60 * 60 * 1000, int(a[1][:-1]) * 60 * 1000, int(a[2][:-1]) * 1000]
    return int(sum(b))


#

# # #
# data, start_date, end_date, startTime, endTime, = data, '2020-10-11 00-00-00', '2020-10-18 00-00-00', \
#                                                   '2020-10-15 00-00-00', '2020-10-15 23-59-59'

startTime, endTime ='2020-10-18 00-00-00', '2020-10-25 00-00-00'


def freqency_matrix_weekly(request, startTime, endTime, regions):
    data = default_getall(regions)

    startTime = dateTOtimestamp(startTime)
    endTime = dateTOtimestamp(endTime)

    users_device_id = {}
    for i, j in data.items():
        users_device_id.update({i: j['device_id']})

    # =================================== day all =========================================

    max_pair1 = {}
    for i, j in data.items():

        sliced_events = []
        contact_events = j['contact_event']
        # if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
        max_pair1.update({i: sliced_events})

    pair_count1 = {}
    for i, j in max_pair1.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({emp_name: {'count': 0}})
        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count1.update({i: temp})

    target_dict = copy.deepcopy(pair_count1)
    f = copy.deepcopy(pair_count1)

    for k in pair_count1.keys():
        for i, j in f[k].items():
            temp_user = i
            temp_user_count = j['count']
            if not i == k:
                user_count = pair_count1[i][k]['count']
                if user_count > temp_user_count:
                    target_dict[k][i]['count'] = user_count

    temp_target = copy.deepcopy(target_dict)
    for i, j in temp_target.items():
        for k in j.keys():
            dev_id = users_device_id[k]
            target_dict[i][dev_id] = target_dict[i].pop(k)

    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}

    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
                event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})
            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})
        max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

        # ========================== merger pair count =========================================
        temp_pair = pair_count
        for u, k in temp_pair.items():
            for i, j in k.items():
                count_1 = j
                count_tar = target_dict[u][i]['count']
                if count_tar > count_1:
                    pair_count[u][i] = count_tar

        final_dict = {}
        for i in max_device:
            first_emp = list(i.keys())[0]
            second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
            final_dict.update({
                first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
            })

        # ================================Duplicate removal  ============================
        no_dublicat_dict = {}
        for i, j in final_dict.items():
            if not j == 0:
                a = i.split(' - ')
                a = natsorted(a)
                if not a[0] == a[1]:
                    a = ' - '.join(a)
                    if a in no_dublicat_dict:
                        if no_dublicat_dict[a] < j:
                            no_dublicat_dict.update({a: j})
                    else:
                        no_dublicat_dict.update({a: j})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        for i, j in no_dublicat_dict.items():
            a1, a2 = i.split(' - ')
            try:
                final_dict[a1][a2] = j
                final_dict[a2][a1] = j
            except:
                pass

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            result_dict[i].update({'others': 0})
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i]['others'] += emp
                    result_dict[i].pop(e)

        team_dict.update({t: result_dict})

    return Response(team_dict)




print(team_dict['Team A'])