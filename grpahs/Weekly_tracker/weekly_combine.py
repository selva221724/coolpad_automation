import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy
import pytz
import sys
timezone = pytz.timezone('America/Chicago')

import requests

import timeit

start = timeit.default_timer()

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
r = requests.get(tyson)
data = r.json()

#==========================================================================
stop = timeit.default_timer()
total_time = stop - start

# output running time in a nice format.
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)

sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))



start = timeit.default_timer()
def dateTOtimestamp(startdate):
    d, t = startdate.split(' ')
    d = d.split('-')
    t = t.split('-')
    event_time = datetime.datetime(int(d[0]), int(d[1]), int(d[2]),
                                   hour=int(t[0]), minute=int(t[1]), second=int(t[2]))

    startTime = timezone.localize(event_time)
    return startTime


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


startTime, endTime, = '2020-10-18 00-00-00', '2020-10-24 23-59-59'



# def weekly_tracker_get(request, startTime, endTime, regions):
#     data = default_getall(regions)

sp = startTime.split(' ')[0]
sp = sp.split('-')
current_date = datetime.datetime(int(sp[0]), int(sp[1]), int(sp[2]), 00, 00, 00)
current_date = timezone.localize(current_date)

startTime = dateTOtimestamp(startTime)
endTime = dateTOtimestamp(endTime)

users_device_id = {}

for i, j in data.items():
    users_device_id.update({i: j['device_id']})


all_events = []
max_pair = {}
for i, j in data.items():
    contact_events = j['contact_event']
    contact_events = [k for n, k in enumerate(contact_events) if k not in contact_events[n + 1:]]
    if not contact_events == []:
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
                all_events.append(event)
        max_pair.update({i: sliced_events})
# ================================ Find the pair counts ================
max_device = []
pair_count = {}
for i, j in max_pair.items():
    temp = {}
    for users in users_device_id.values():
        temp.update({users: 0})
    for event in j:
        temp[event['remote']] += 1
    pair_count.update({i: temp})
    max_key = max(temp, key=lambda k: temp[k])
    max_device.append({i: {max_key: temp[max_key]}})
max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)


# ===================================== pair count ============================================

final_dict = {}
for i, j in pair_count.items():
    first_emp = i
    for k, l in j.items():
        second_emp = [m for m, n in users_device_id.items() if n == k][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: l
        })

# ================================Duplicate removal  ============================
no_dublicat_dict = {}
for i, j in final_dict.items():
    if not j == 0:
        a = i.split(' - ')
        a = natsorted(a)
        if not a[0] == a[1]:
            a = ' - '.join(a)
            if a in no_dublicat_dict:
                if no_dublicat_dict[a] < j:
                    no_dublicat_dict.update({a: j})
            else:
                no_dublicat_dict.update({a: j})

no_dublicat_dict = dict(sorted(no_dublicat_dict.items(), key=lambda x: x[1], reverse=True))

contact_history_all = copy.deepcopy(no_dublicat_dict)

# ================================ Get the Top Pairs ============================
top_pairs_number = 5  # number of top pairs needed
top_pairs = {}
count = 0
for i, j in no_dublicat_dict.items():
    if count == top_pairs_number:
        break
    top_pairs.update({i: j})
    count += 1

# ================================Duplicate removal  ============================

duplicate_events = copy.deepcopy(all_events)
for i in duplicate_events:
    local = i['local']
    remote = i['remote']
    local_time = i['start']
    for j in duplicate_events:
        local_temp = j['local']
        remote_temp = j['remote']
        remote_time = j['start']
        if local == remote_temp and remote == local_temp:
            tim_diff = int(local_time) - int(remote_time)
            e1 = datetime.datetime.utcfromtimestamp(int(local_time) / 1000)
            e1 = e1.replace(tzinfo=pytz.utc).astimezone(timezone)

            e2 = datetime.datetime.utcfromtimestamp(int(remote_time) / 1000)
            e2 = e2.replace(tzinfo=pytz.utc).astimezone(timezone)

            g = e1 - e2
            if g.days == -1:
                g = e2 - e1

            if g.seconds < 2:
                try:
                    duplicate_events.remove(j)
                except:
                    pass

result_total_contacts = len(duplicate_events)
# result_total_contacts = len(duplicate_events)

# ============================================== freqency_matrix_weekly =================================
# def freqency_matrix_weekly(request, startTime,endTime,regions):
team_list = [j['team'] for i, j in data.items()]
team_list = natsorted(list(set(team_list)))
data_team = {}
for team in team_list:
    temp_data = {}
    for i, j in data.items():
        temp_team = j['team']
        if temp_team == team:
            temp_data.update({i: j})
    data_team.update({team: temp_data})
# return Response(team_dict)
# ============================ weekly_team_tracker ==========================
# def weekly_team_tracker(request, startTime,endTime,regions):
team_dict = {}
for t, team in data_team.items():
    all_events = []
    for i, j in team.items():
        contact_events = j['contact_event']
        sliced_events = []
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
            event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
                all_events.append(event)
    # ================================Duplicate removal  ============================
    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = int(local_time) - int(remote_time)
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)
    result = len(all_events)
    team_dict.update({t: result})
# return Response(team_dict)
# ==================================== clk_chart_weekly =============================================
# def clk_chart_weekly(request,current_date,regions):

hours_list = []
hours_list.append(current_date)
for i in range(7):
    mints_back = current_date + datetime.timedelta(days=i + 1)
    hours_list.append(mints_back)
hour_wise_count = []
for hr in range(len(hours_list) - 1):
    mints_back = hours_list[hr]
    current_date_1 = hours_list[hr + 1]
    # ================================ slice the all events by given date================
    all_events = []

    for event in duplicate_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if isNowInTimePeriod(mints_back, current_date_1, event_time):
            all_events.append(event)

    result = len(all_events)
    hour_wise_count.append(result)
# return Response(hour_wise_count)
weekly_tracker_dict = {
    'pairs_history_weekly': contact_history_all,
    'top_pairs_maxmium_contact_week': top_pairs,
    'total_no_of_contacts_weekly': result_total_contacts,
    'weekly_team_tracker': team_dict,
    'clk_chart_weekly': hour_wise_count
}

    # return Response(weekly_tracker_dict)


print(weekly_tracker_dict)


#==========================================================================
stop = timeit.default_timer()
total_time = stop - start

# output running time in a nice format.
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)

sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))
