import copy
import datetime
import json
import random

import dateutil.relativedelta
import pytz
from natsort import natsorted

# {'local': '90030000783690102001',
#   'remote': '90030000783690102002',
#   'start': 1601996269574,
#   'duration': 7780,
#   'avgDist': 215,
#   'minDist': 205}

timezone = pytz.timezone('America/Chicago')

# with open(
#         'data.json') as f:
#     data = json.load(f)

import requests

tyson = "https://takvaviya.in/coolpad_backend/user/getall/Tyson__America__South__Tyson?format=json"
mdmd = "https://takvaviya.in/coolpad_backend/user/getall/PWD__INDIA__South__Coolpad?format=json"
r = requests.get(mdmd)
data = r.json()


def cm_to_ft(val):
    import quantities as pq
    distance = val * pq.cm
    distance.units = 'feet'
    distance = round(float(distance), 4)
    distance = str(distance) + ' ft'
    # print(distance,"thambi")
    return distance


startTime, endTime, current_date = '2020-10-04', '2020-10-10', '2020-10-09'


# ================================ Get Users vs device id ===============================

# users_device_id = {}
# for i, j in data.items():
#     users_device_id.update({i: j['device_id']})


def dateTOtimestamp(startdate):
    t = startdate.split('-')
    event_time = datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
    startTime = event_time.replace(tzinfo=timezone).astimezone(timezone)
    return startTime


# 2020-09-13/2020-09-18/2020-09-16/  -Start/End/Current

# startTime = dateTOtimestamp('020-09-13')
# endTime = dateTOtimestamp('2020-09-18')
# current_date = dateTOtimestamp('2020-09-16')

def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


# data,startTime, endTime, current_date = data,'2020-09-13','2020-09-18','2020-09-17'
# u = 'E-11'
# user = data['E-11']

# def User_history_data(data, startTime, endTime, current_date):
# data = default_getall(regions)
startTime = dateTOtimestamp(startTime)
endTime = dateTOtimestamp(endTime)
users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

user_data = {}
current_date = dateTOtimestamp(current_date)
# for u, user in data.items():

u = 'E-1'
user = data[u]

device_id = user['device_id']
contact_events = user['contact_event']
contact_events.append({'local': '90030000783690102203',
  'remote': '90030000783690102204',
  'start': 1602227483999,
  'duration': 0,
  'avgDist': 1978,
  'minDist': 1978})

sliced_events = []

if not contact_events == []:
    for event in contact_events:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if current_date.date() == event_time.date():
            sliced_events.append(event)

sliced_events_by_user = {}
for i, j in users_device_id.items():
    temp = []
    for eve in sliced_events:
        if eve['remote'] == j:
            temp.append(eve)
    sliced_events_by_user.update({i: temp})

opposite_events = {}
for i, j in data.items():
    temp_eve = []
    opp_evs = j['contact_event']
    for event in opp_evs:
        event_time_epoch = event['start']
        event_time = datetime.datetime.utcfromtimestamp(int(event_time_epoch) / 1000)
        event_time = event_time.replace(tzinfo=pytz.utc).astimezone(timezone)
        if current_date.date() == event_time.date():
            if event['remote'] == device_id:
                temp_eve.append(event)
    opposite_events.update({i: temp_eve})

updated_events = {}
for i, j in opposite_events.items():
    comapre_events = sliced_events_by_user[i]
    if not j == []:
        if not len(comapre_events) == len(j):
            if len(comapre_events) < len(j):
                d = []
                for k in j:
                    k['remote'] = k['local']
                    k['local'] = device_id
                    d.append(k)
                updated_events.update({i: d})

for i, j in updated_events.items():
    for eve in j:
        sliced_events_by_user[i].append(eve)

comb_all = []


for ii, jj in sliced_events_by_user.items():
    if not jj == []:
        temp_event = sorted(jj, key=lambda x: x['start'])

        dup_event = copy.deepcopy(i)
        comb_event = []
        temp_comb = [temp_event[0]]

        for e in range(1, len(temp_event)):
            ts_1 = temp_event[e - 1]['start']
            duration_1 = temp_event[e - 1]['duration']

            buffer_seconds = 10
            ts_1 = datetime.datetime.utcfromtimestamp(int(ts_1) / 1000)
            ts_1 = ts_1.replace(tzinfo=pytz.utc).astimezone(timezone)
            exp_time = ts_1 + datetime.timedelta(seconds=int(duration_1 / 1000) + buffer_seconds)

            ts_2 = temp_event[e]['start']
            duration_2 = temp_event[e]['duration']
            ts_2 = datetime.datetime.utcfromtimestamp(int(ts_2) / 1000)
            ts_2 = ts_2.replace(tzinfo=pytz.utc).astimezone(timezone)

            tim_diff = exp_time - ts_2

            if tim_diff.days == 0 and tim_diff.seconds < 10:
                temp_comb.append(temp_event[e])
            else:
                comb_event.append(temp_comb)
                temp_comb = [temp_event[e]]

        comb_event.append(temp_comb)
        comb_all.append(comb_event)


final_events = []
for eve in comb_all:
    for i in eve:
        if len(i) == 0:
            final_events.append(i)
        else:
            t = i[0]
            final_events.append(t)

for i in final_events:
    if not i in sliced_events:
        sliced_events.append(i)
#
print(len(sliced_events))