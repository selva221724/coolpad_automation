import boto3
import json

sqs = boto3.client('sqs', aws_access_key_id=None, aws_secret_access_key=None, endpoint_url='https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad')
queue_url = 'https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad'


while True:
    messages = sqs.receive_message(QueueUrl=queue_url,AttributeNames=['SentTimestamp'],MaxNumberOfMessages=1,
                                   MessageAttributeNames=['All'],VisibilityTimeout=0,WaitTimeSeconds=5)

    if not list(messages.keys())[0]=='ResponseMetadata':

        recived_msg = json.loads(messages['Messages'][0]['Body'])
        receipt_handle =  messages['Messages'][0]['ReceiptHandle']
        sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )
        print(recived_msg)