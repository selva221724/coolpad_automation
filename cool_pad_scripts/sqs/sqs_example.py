import boto3
import json
# sqs = boto3.resource('sqs')

# Get the queue. This returns an SQS.Queue instance
# queue = sqs.get_queue_by_name(QueueName='cool_pad')
sqs = boto3.client('sqs', aws_access_key_id=None, aws_secret_access_key=None, endpoint_url='https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad')
queue_url = 'https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad'
response = sqs.send_message(
    QueueUrl=queue_url,
    MessageBody=json.dumps(
    {"local": "123456789A123456789B", "remote": "234567890A234567890C", "start": 1595247300001, "duration": 300000,
     "avgDist": 1676, "minDist": 838})
)

sqs = boto3.client('sqs')
messages = sqs.receive_message(QueueUrl=queue_url)
recived_msg = json.loads(messages['Messages'][0]['Body'])