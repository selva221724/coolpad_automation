import boto3
import json
import random
import time
import datetime
import random
from random import randrange

# sqs = boto3.resource('sqs')

# Get the queue. This returns an SQS.Queue instance
# queue = sqs.get_queue_by_name(QueueName='cool_pad')
sqs = boto3.client('sqs', aws_access_key_id=None, aws_secret_access_key=None,
                   endpoint_url='https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad')

queue_url = 'https://sqs.us-east-1.amazonaws.com/449696532783/cool_pad'

users = ['023456789A123456789B', '123456789A123456789B', '223456789A123456789B', '323456789A123456789B',
         '423456789A123456789B', '523456789A123456789B', '623456789A123456789B', '723456789A123456789B',
         '823456789A123456789B', '923456789A123456789B', '1023456789A123456789B', '1123456789A123456789B',
         '1223456789A123456789B', '1323456789A123456789B', '1423456789A123456789B', '1523456789A123456789B',
         '1623456789A123456789B', '1723456789A123456789B', '1823456789A123456789B', '1923456789A123456789B']


def unix_time_millis(dt):
    return int((dt - epoch).total_seconds() * 1000)


epoch = datetime.datetime.utcfromtimestamp(0)
startDate = datetime.datetime(2020, 7, 20, 12, 00, 00)

while True:

    local = random.choice(users)
    remote = random.choice(users)
    if local == remote:
        remote = random.choice(users)
    avgDist = random.randint(900, 1798)
    minDist = random.randint(400, 900)
    duration = random.randint(300000, 3000000)
    current = startDate + datetime.timedelta(hours=randrange(-5, 5)) + \
              datetime.timedelta(seconds=randrange(-10, 10)) + \
              datetime.timedelta(minutes=randrange(10))
    start_time = unix_time_millis(current)
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody=json.dumps(
            {"local": local, "remote": remote, "start": start_time,
             "duration": duration,
             "avgDist": avgDist, "minDist": minDist}),

        MessageAttributes={
            'Title': {
                'DataType': 'String',
                'StringValue': 'The Whistler'
            },
            'Author': {
                'DataType': 'String',
                'StringValue': 'John Grisham'
            },
            'WeeksOn': {
                'DataType': 'Number',
                'StringValue': '6'
            }
        }
    )
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody=json.dumps(
            {"local": remote, "remote": local, "start": start_time + 5,
             "duration": duration,
             "avgDist": avgDist, "minDist": minDist}),

        MessageAttributes={
            'Title': {
                'DataType': 'String',
                'StringValue': 'The Whistler'
            },
            'Author': {
                'DataType': 'String',
                'StringValue': 'John Grisham'
            },
            'WeeksOn': {
                'DataType': 'Number',
                'StringValue': '6'
            }
        }
    )

    print('Message_sent')
    time.sleep(5)
