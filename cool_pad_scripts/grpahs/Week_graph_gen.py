import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})


# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime= datetime.datetime(2020, 8, 7, 12, 00, 00)

def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else: #Over midnight
        return nowTime >= startTime or nowTime <= endTime

def top_pairs_maxmium_contact(data,startTime,endTime, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        a = ' - '.join(a)
        no_dublicat_dict.update({a: j})

    # ================================ Get the Top Pairs ============================
    top_pairs_number = 5  # number of top pairs needed

    top_pairs = {}
    count = 0
    for i, j in no_dublicat_dict.items():
        if count == top_pairs_number:
            break
        top_pairs.update({i: j})
        count += 1

    return top_pairs


top_pairs = top_pairs_maxmium_contact(data, startTime,endTime, users_device_id)


# ==============================================================================================
# ================================= All contact pair count in a week =====================
startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime= datetime.datetime(2020, 8, 7, 12, 00, 00)

def pairs_maxmium_contact(data, startTime,endTime, users_device_id):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================ Find the pair counts ================
    max_device = []
    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            temp.update({users: 0})
        for event in j:
            temp[event['remote']] += 1
        pair_count.update({i: temp})

        max_key = max(temp, key=lambda k: temp[k])
        max_device.append({i: {max_key: temp[max_key]}})

    # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

    # ================================ matching back the device id with user id  ================
    final_dict = {}
    for i in max_device:
        first_emp = list(i.keys())[0]
        second_emp = [k for k, v in users_device_id.items() if v == list(list(i.values())[0].keys())[0]][0]
        final_dict.update({
            first_emp + ' - ' + second_emp: list(list(i.values())[0].values())[0]
        })

    # ================================Duplicate removal  ============================
    no_dublicat_dict = {}
    for i, j in final_dict.items():
        a = i.split(' - ')
        a = natsorted(a)
        a = ' - '.join(a)
        no_dublicat_dict.update({a: j})

    return no_dublicat_dict


pairs_history = pairs_maxmium_contact(data, startTime,endTime, users_device_id)



# ==============================================================================================
# ================================== Total Number of Contacts Today in a week  ======================

startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime= datetime.datetime(2020, 8, 7, 12, 00, 00)

def total_no_of_contacts(data, startTime,endTime):
    # ================================ slice the all events by given date================
    all_events = []
    max_pair = {}
    for i, j in data.items():
        contact_events = j['contact_event']
        if not contact_events == []:
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

    # ================================Duplicate removal  ============================

    for i in all_events:
        local = i['local']
        remote = i['remote']
        local_time = i['start']
        for j in all_events:
            local_temp = j['local']
            remote_temp = j['remote']
            remote_time = j['start']
            if local == local_temp and remote == remote_temp:
                tim_diff = local_time - remote_time
                if -1000 < tim_diff < 1000:
                    all_events.remove(j)

    result = len(all_events)

    return result


total_no_contacts = total_no_of_contacts(data, startTime,endTime)


# ==============================================================================================
# ===================================== Contact freqency matrix 40x40 in a week ============================
startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime= datetime.datetime(2020, 8, 7, 12, 00, 00)

def freqency_matrix(data, startTime,endTime, users_device_id):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []
        max_pair = {}
        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)
            max_pair.update({i: sliced_events})

        # ================================ Find the pair counts ================

        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

        # ================================ matching back the device id with user id  ================
        final_dict = {}
        for i, j in pair_count.items():
            temp = {}
            for emp, k in j.items():
                emp_id = [k for k, v in users_device_id.items() if v == emp][0]
                temp.update({emp_id: k})
            final_dict.update({i: temp})

        temp_team_list = list(final_dict.keys())
        result_dict = copy.deepcopy(final_dict)
        for i, j in final_dict.items():
            for e, emp in j.items():
                if not e in temp_team_list:
                    result_dict[i].pop(e)

        team_dict.update({t: result_dict})

    return team_dict


freq_matrx = freqency_matrix(data, startTime,endTime, users_device_id)

# ====================================================================================================
# ============================== weekly team tracker count ===========================================

startTime = datetime.datetime(2020, 8, 3, 12, 00, 00)
endTime= datetime.datetime(2020, 8, 7, 12, 00, 00)

def weekly_team_tracker(data, startTime,endTime):
    team_list = [j['team'] for i, j in data.items()]
    team_list = natsorted(list(set(team_list)))

    data_team = {}
    for team in team_list:
        temp_data = {}
        for i, j in data.items():
            temp_team = j['team']
            if temp_team == team:
                temp_data.update({i: j})
        data_team.update({team: temp_data})

    team_dict = {}
    for t, team in data_team.items():

        all_events = []

        for i, j in team.items():
            contact_events = j['contact_event']
            sliced_events = []
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                    all_events.append(event)

        # ================================Duplicate removal  ============================

        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = local_time - remote_time
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)

        result = len(all_events)

        team_dict.update({t:result})

    return team_dict

weekly_team_count = weekly_team_tracker(data, startTime,endTime)


# ===================================================================================
# ========================================= week clock chart =================================================


current_date = datetime.datetime(2020, 8, 3, 12, 00, 00) # this is monday

def clk_chart(data,current_date):

    current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 00, 00)

    hours_list=[]
    hours_list.append(current_date)
    for i in range(5):
        mints_back = current_date + datetime.timedelta(days=i+1)
        hours_list.append(mints_back)


    hour_wise_count = []
    for hr in range(len(hours_list)-1):

        mints_back = hours_list[hr]
        current_date = hours_list[hr+1]
        # ================================ slice the all events by given date================
        all_events = []
        max_pair = {}
        for i, j in data.items():
            contact_events = j['contact_event']
            if not contact_events == []:
                sliced_events = []
                for event in contact_events:
                    event_time_epoch = event['start']
                    event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                    if isNowInTimePeriod(mints_back,current_date,event_time):
                        sliced_events.append(event)
                        all_events.append(event)
                max_pair.update({i: sliced_events})

        # ================================Duplicate removal  ============================

        for i in all_events:
            local = i['local']
            remote = i['remote']
            local_time = i['start']
            for j in all_events:
                local_temp = j['local']
                remote_temp = j['remote']
                remote_time = j['start']
                if local == local_temp and remote == remote_temp:
                    tim_diff = local_time - remote_time
                    if -1000 < tim_diff < 1000:
                        all_events.remove(j)

        result = len(all_events)
        hour_wise_count.append(result)

    return hour_wise_count

clock_chart = clk_chart(data,current_date)

