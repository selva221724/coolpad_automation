import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================

current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


for e,emp in data:

    events = emp['contact_event']
    #
    # events = [
    #     {"local": "8912230200020077083f", "remote": "89011703278216971203", "start": 1596748151164, "duration": 300000,
    #      "avgDist": 1280, "minDist": 762},
    #     {"local": "8912230200020077083f", "remote": "89011703278216971203", "start": 1596748156164, "duration": 135000,
    #      "avgDist": 1280, "minDist": 762}]


    for i in events:
        local = i['local']
        remote =i['remote']
        start = i['start']
        dur = i['duration']
        event_time = datetime.datetime.utcfromtimestamp(start / 1000)
        for j in events:
            local_temp = j['local']
            remote_temp = j['remote']
            start_temp = j['start']
            dur_temp = j['duration']
            if local==local_temp and remote == remote_temp:
                time_dif = start_temp-start
                if time_dif > 10:
                    events.remove(i)
                    events.remove(j)
                    duration_time = dur+dur_temp
                    avgdist = int(i['avgDist']+j['avgDist']/2)
                    mindist = min([i['minDist'],j['minDist']])
                    events.append({"local": local_temp, "remote": remote_temp, "start": start, "duration": duration_time,
         "avgDist": avgdist, "minDist": mindist})


    data[emp]['contact_event'] = events
