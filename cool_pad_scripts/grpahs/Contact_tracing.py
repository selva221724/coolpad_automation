import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a

primery_target = 'emp_1'

startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)

def get_top_five_tree(data,primery_target,startTime,endTime,users_device_id):


    max_pair = {}
    sliced_events = []
    contact_events = data[primery_target]['contact_event']
    if not contact_events == []:
        for event in contact_events:
            event_time_epoch = event['start']
            event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
            if isNowInTimePeriod(startTime, endTime, event_time):
                sliced_events.append(event)
            max_pair.update({primery_target: sliced_events})

    pair_count = {}
    for i, j in max_pair.items():
        temp = {}
        for users in users_device_id.values():
            emp_name = [a for a, v in users_device_id.items() if v == users][0]
            temp.update({emp_name: {'count': 0, 'max_duration': []}})
        for event in j:
            remote = [a for a, v in users_device_id.items() if v == event['remote']][0]
            temp[remote]['count'] += 1
            temp[remote]['max_duration'].append(event['duration'])

        for t, tt in temp.items():
            try:
                max_dur = max(tt['max_duration'])
                temp[t]['max_duration'] = ms_to_time(max_dur)
            except:
                temp[t]['max_duration'] = None

        temp = dict(sorted(temp.items(), key=lambda x: x[1]['count'], reverse=True))

        pair_count.update({i: temp})

    top_five = dict(list(list(pair_count.values())[0].items()))
    result_dict = copy.deepcopy(top_five)
    for i, j in top_five.items():
        if j['count'] == 0:
            result_dict.pop(i)

    return {primery_target:result_dict}


def contact_trace(data,primery_target,startTime,endTime, users_device_id):


    final_list =[]

    top_five = get_top_five_tree(data, primery_target, startTime, endTime, users_device_id)
    final_list.append(top_five)
    # tree_dict = copy.deepcopy(top_five)

    for i in top_five.values():
        temp=[]
        for j,k in i.items():
            primery_target = j
            top_five = get_top_five_tree(data, primery_target, startTime, endTime, users_device_id)
            temp.append(top_five)

    final_list.append(temp)
    return final_list




trace = contact_trace(data, primery_target, startTime, endTime, users_device_id)
