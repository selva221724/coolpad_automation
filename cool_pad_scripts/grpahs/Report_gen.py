import json
import random
import datetime
from natsort import natsorted
import dateutil.relativedelta
import copy

with open(
        '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Sample_data_generation/data.json') as f:
    data = json.load(f)

# ================================ Get Users vs device id ===============================

users_device_id = {}
for i, j in data.items():
    users_device_id.update({i: j['device_id']})

# ==============================================================================================
# ================================= Top 5 pairs in a maximum contact pair in a week =====================
startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else:  # Over midnight
        return nowTime >= startTime or nowTime <= endTime


def ms_to_time(millis):
    millis = int(millis)
    seconds = (millis / 1000) % 60
    seconds = int(seconds)
    minutes = (millis / (1000 * 60)) % 60
    minutes = int(minutes)
    hours = (millis / (1000 * 60 * 60)) % 24
    hours = int(hours)
    a = str(hours) + ':' + str(minutes) + ':' + str(seconds)
    return a


def User_history_data(data,startTime, endTime, current_date,users_device_id):

    user_data = {}
    for u, user in data.items():
        device_id = user['device_id']
        contact_events = user['contact_event']

        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if current_date.date() == event_time.date():
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})

        # max_device = sorted(max_device, key=lambda x: list(list(x.values())[0].values())[0], reverse=True)

        # ========================= no of contacts in single day =============================
        no_of_contacts = 0
        for i in pair_count.values():
            for j in i.values():
                if not j==0:
                    no_of_contacts += 1

        # ============================== max contact duration with user ===================

        max_duration = max([[j['duration'] for j in i] for i in max_pair.values()][0])
        max_duration = ms_to_time(max_duration)

        # ============================= contact history single day  ==========================

        contact_history = {}
        for i in pair_count.values():
            for j, k in i.items():
                if not k == 0:
                    second_emp = [a for a, v in users_device_id.items() if v == j][0]
                    contact_history.update({second_emp: k})

        # =========================================================================================
        # ================================== week wise ============================================
        max_pair = {}
        sliced_events = []
        if not contact_events == []:
            for event in contact_events:
                event_time_epoch = event['start']
                event_time = datetime.datetime.utcfromtimestamp(event_time_epoch / 1000)
                if isNowInTimePeriod(startTime, endTime, event_time):
                    sliced_events.append(event)
                max_pair.update({u: sliced_events})

        max_device = []
        pair_count = {}
        for i, j in max_pair.items():
            temp = {}
            for users in users_device_id.values():
                temp.update({users: 0})
            for event in j:
                temp[event['remote']] += 1
            pair_count.update({i: temp})

            max_key = max(temp, key=lambda k: temp[k])
            max_device.append({i: {max_key: temp[max_key]}})

        # ========================= no of contacts in week =============================
        no_of_contacts_week = 0
        for i in pair_count.values():
            for j in i.values():
                if not j == 0:
                    no_of_contacts_week += 1

        # ============================== max contact duration with user week ===================

        max_duration_week = max([[j['duration'] for j in i] for i in max_pair.values()][0])
        max_duration_week = ms_to_time(max_duration_week)

        # ============================= contact history week  ==========================

        contact_history_week = {}
        for i in pair_count.values():
            for j, k in i.items():
                if not k == 0:
                    second_emp = [a for a, v in users_device_id.items() if v == j][0]
                    contact_history_week.update({second_emp: k})

        # =============================== Final dict append =========================

        user_data.update({u:{
            'Current Device ID': device_id,
            'Users in Contact Day': no_of_contacts,
            'Users in Contact Week': no_of_contacts_week,
            'Max Contact Duration Day': max_duration,
            'Max Contact Duration Week':max_duration_week,
            'Contact History Day': contact_history,
            'Contact History Week': contact_history_week

        }})

    return user_data

userdata = User_history_data(data,startTime,endTime,current_date,users_device_id)


user = 'emp_1'
d = userdata['emp_1']

d[ 'No of Users in Contact per Day'] = d.pop( 'Users in Contact Day')
d[ 'No of Users in Contact per Week'] = d.pop( 'Users in Contact Week')
d[ 'Maximum Contact Duration per Day'] = d.pop( 'Max Contact Duration Day')
d[ 'Maximum Contact Duration per Week'] = d.pop( 'Max Contact Duration Week')
d[ 'Contact History per Day'] = d.pop( 'Contact History Day')
d[ 'Contact History per Week'] = d.pop( 'Contact History Week')


import pandas as pd
import csv

df = pd.DataFrame(d.items(),columns=[None,user])
# df = df.T

df.to_csv(user+' History.csv',index=False)

df.to_excel(user+' History.xlsx',index=False)

from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter , A4
from reportlab.platypus import SimpleDocTemplate,Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet

startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def pdf_gen(csv_file,current_date,user):
    # Introduction text
    line1 = 'Report for '+user
    line2 = 'Date: ' +current_date.strftime("%B %d %Y")

    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    # File that must be written to report
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)

    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista][1:]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    elements = []
    archivo_pdf = SimpleDocTemplate(user+' History.pdf', pagesize=A4, rightMargin=30, leftMargin=30, topMargin=40, bottomMargin=28)

    im = Image('logo.png',width=230,height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)


    archivo_pdf.build(elements)

pdf_gen(user+' History.csv',current_date,user)

# ============================== team history ================================


team_history = {'A': {'Users in Contact': 15, 'Contacts in a Day': 112, 'Contacts in a Week': 484, 'top 5 users in contact Day': {'emp_17': 26, 'emp_4': 24, 'emp_2': 21, 'emp_3': 21, 'emp_1': 20}, 'top 5 users in contact Week': {'emp_1': 109, 'emp_4': 104, 'emp_2': 96, 'emp_3': 90, 'emp_17': 85}, 'Most contact user pair Day': {'emp_1 - emp_18': 3}, 'Most contact user pair Week': {'emp_1 - emp_18': 15}}, 'B': {'Users in Contact': 15, 'Contacts in a Day': 120, 'Contacts in a Week': 490, 'top 5 users in contact Day': {'emp_18': 35, 'emp_5': 27, 'emp_8': 27, 'emp_7': 18, 'emp_6': 13}, 'top 5 users in contact Week': {'emp_18': 117, 'emp_5': 105, 'emp_7': 99, 'emp_8': 85, 'emp_6': 84}, 'Most contact user pair Day': {'emp_5 - emp_10': 5}, 'Most contact user pair Week': {'emp_5 - emp_6': 11}}, 'C': {'Users in Contact': 15, 'Contacts in a Day': 137, 'Contacts in a Week': 501, 'top 5 users in contact Day': {'emp_10': 32, 'emp_12': 29, 'emp_11': 27, 'emp_9': 26, 'emp_19': 23}, 'top 5 users in contact Week': {'emp_12': 110, 'emp_10': 105, 'emp_11': 105, 'emp_9': 93, 'emp_19': 88}, 'Most contact user pair Day': {'emp_9 - emp_13': 4}, 'Most contact user pair Week': {'emp_9 - emp_1': 10}}, 'D': {'Users in Contact': 15, 'Contacts in a Day': 131, 'Contacts in a Week': 525, 'top 5 users in contact Day': {'emp_13': 31, 'emp_16': 29, 'emp_20': 28, 'emp_14': 24, 'emp_15': 19}, 'top 5 users in contact Week': {'emp_20': 120, 'emp_15': 104, 'emp_14': 103, 'emp_16': 102, 'emp_13': 96}, 'Most contact user pair Day': {'emp_13 - emp_9': 4}, 'Most contact user pair Week': {'emp_13 - emp_9': 9}}}




team = 'A'
d = team_history['A']

d[ 'No of Users in Contact per Day'] = d.pop( 'Users in Contact')
d[ 'No of Contact in a Day'] = d.pop( 'Contacts in a Day')
d[ 'No of Contact in a Week'] = d.pop( 'Contacts in a Week')
d[ 'Top 5 Users in contact in a Day'] = d.pop( 'top 5 users in contact Day')
d[ 'Top 5 Users in contact in a Week'] = d.pop( 'top 5 users in contact Week')
d[ 'Most contact user pair in a Day'] = d.pop( 'Most contact user pair Day')
d[ 'Most contact user pair in a Week'] = d.pop( 'Most contact user pair Week')


import pandas as pd
import csv

df = pd.DataFrame(d.items(),columns=[None,team])
# df = df.T

df.to_csv(team+'-Team History.csv',index=False)

df.to_excel(team+'-Team History.xlsx',index=False)

from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter , A4
from reportlab.platypus import SimpleDocTemplate,Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet

startTime = datetime.datetime(2020, 8, 3, 0, 00, 00)
endTime = datetime.datetime(2020, 8, 7, 0, 00, 00)
current_date = datetime.datetime(2020, 8, 4, 12, 00, 00)


def pdf_gen_team(csv_file,current_date,team):
    # Introduction text
    line1 = 'Report for Team-'+team
    line2 = 'Date: ' +current_date.strftime("%B %d %Y")

    # PDF document layout
    table_style = TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'CENTER'),
        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
        ('VALIGN', (0, 0), (0, -1), 'CENTER'),
        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
    ])

    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading1']
    styleHeading2 = styles['Heading2']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1  # centre text (TA_CENTRE)

    # Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    # File that must be written to report
    with open(csv_file, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)

    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista][1:]
    t = Table(data2)
    t.setStyle(table_style)
    # t._argW[0] = 0.7 * inch

    elements = []
    archivo_pdf = SimpleDocTemplate(team+'-Team History.pdf', pagesize=A4, rightMargin=30, leftMargin=30, topMargin=40, bottomMargin=28)

    im = Image('logo.png',width=230,height=100)
    elements.append(im)
    elements.append(Spacer(inch, .25 * inch))
    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25 * inch))
    elements.append(t)


    archivo_pdf.build(elements)

pdf_gen_team(team+'-Team History.csv',current_date,user)