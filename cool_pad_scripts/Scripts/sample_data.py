import json

sample_dict = {
        'User 1': {
            'user_id': 'A1',
            'device_id': '123456789A123456789B',
            'history_of_ids': ['123456789A123456789B', "234567890A234567890C"],
            'team':'X1',
            'group':'Y1',
            'locations':'block 1',
            'zone': '1',
            'company': 'Tyson',
            'hr_notes': ['xyz', 'xxx', 'hhh'],
            'events':{
                'contact_event':[
                    {"local": "123456789A123456789B","remote": "234567890A234567890C",
                     "start": 1595247300001,"duration": 300000,"avgDist": 1676,"minDist": 838}],
                'heartbeat_event':[
                    {"local": "123456789A123456789B","tstamp": 1595247300001,"battery": 852,"shutdown": true}]
                    }
                }
            }
