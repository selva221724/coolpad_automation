import geo
from pykml import parser
import re
import numpy as np
from matplotlib import pyplot as plt
import simplekml
from shapely.geometry import Point
import pyproj
from pyproj import Proj, transform
from osgeo import gdal
from pandas import DataFrame
from haversine import haversine, Unit


kml_path = '/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Data/Worker_data/Worker_1.kml'


kml,des = geo.kread(kml_path,description=True)



def lerp(v0, v1, i):
    return v0 + i * (v1 - v0)

def getEquidistantPoints(p1, p2, n):
    return [[lerp(p1[0],p2[0],1./n*i), lerp(p1[1],p2[1],1./n*i)] for i in range(n+1)]

def extract_info(data):
    all_data=[]
    for i in range(len(data)-1):
        a = data[i]
        b = data[i+1]
        data_points = getEquidistantPoints(a,b, 100)
        for j in data_points:
            all_data.append(j)

    data_points =400
    intervel = round(len(all_data)/data_points)-1

    final_data=[]
    for i in range(0,len(all_data),intervel):
        final_data.append(all_data[i])

    extra_points = len(final_data)-data_points
    import random
    for i in range(extra_points):
        final_data.remove(random.choice(final_data))

    return final_data

def kml_read(kml_path):
    kml, des = geo.kread(kml_path, description=True)
    temp=[]
    for i,j in zip(kml,des):
        f = extract_info(i)
        temp.append(f)
    return temp

import glob

kml_list = glob.glob('/mnt/dash/Alpha_Share/Automation_Team/Tamil/IOT Industrial Monitoring/Data/Worker_data/*.kml')

coords=[]
for i in kml_list:
    coords.append(kml_read(i))

one_day_comapre=[]
for i in coords:
    one_day_comapre.append(i[0])

pair_w=0
pair_v =0

sdv=[]
sdw=[]
for i in range(len(one_day_comapre)):
    temp_voil=0
    temp_warn=0
    primery = one_day_comapre[i]
    for j in one_day_comapre:
        for k in range(len(j)):
            p1 = primery[k]
            p2 = j[k]
            if p1==p2:
                break
            dist = haversine(p1, p2, unit='ft')
            if dist < 3:
                temp_voil+=1
            if 0 < dist < 6:
                temp_warn+=1




    sdv.append(temp_voil)
    sdw.append(temp_warn)

print(sum(sdw))
print(sdw)
print(sum(sdv))
print(sdv)

a=[]
for k in range(7):
    one_day_comapre=[]
    for i in coords:
        one_day_comapre.append(i[k])
    a.append(one_day_comapre)


final_w=[]
final_v=[]
for coo in a:
    one_day_comapre = coo
    w=[]
    v=[]
    for i in range(len(one_day_comapre)):
        temp_w =[]
        temp_v = []
        prime = one_day_comapre[i]
        for j in one_day_comapre:
            pair_w = 0
            pair_v = 0
            for k in range(len(j)):
                p1 = prime[k]
                p2 = j[k]
                if p1==p2:
                    break
                dist = haversine(p1, p2, unit='ft')
                if dist < 3:
                    pair_v+=1
                if 0 < dist < 6:
                    pair_w+=1
            temp_w.append(pair_w)
            temp_v.append(pair_v)
        w.append(temp_w)
        v.append(temp_v)
    final_v.append(v)
    final_w.append(w)

temp_w=[]
for k in range(10):
    temp=[]
    for i in final_w:
        temp.append(i[k])
    temp_w.append(temp)


sum_w=[]
for a in temp_w:
    # a = temp_w[0]

    d=[]
    for k in range(10):
        temp=[]
        for i in a:
            temp.append(i[k])
        d.append(sum(temp))
    sum_w.append(d)



v = [[0, 343, 406, 356, 286, 236, 370, 283, 395, 330],
 [343, 0, 205, 286, 247, 381, 217, 288, 348, 225],
 [406, 205, 0, 388, 247, 183, 446, 372, 346, 322],
 [356, 286, 388, 0, 348, 291, 632, 365, 251, 318],
 [286, 247, 247, 348, 0, 375, 269, 263, 213, 318],
 [236, 381, 183, 291, 375, 0, 268, 128, 323, 202],
 [370, 217, 446, 632, 269, 268, 0, 305, 262, 670],
 [283, 288, 372, 365, 263, 128, 305, 0, 285, 279],
 [395, 348, 346, 251, 213, 323, 262, 285, 0, 283],
 [330, 225, 322, 318, 318, 202, 670, 279, 283, 0]]

W =[[0, 582, 825, 808, 498, 515, 749, 539, 691, 569],
 [582, 0, 454, 572, 601, 704, 508, 535, 589, 442],
 [825, 454, 0, 681, 508, 382, 692, 694, 694, 714],
 [808, 572, 681, 0, 690, 540, 1245, 687, 487, 568],
 [498, 601, 508, 690, 0, 744, 544, 482, 419, 574],
 [515, 704, 382, 540, 744, 0, 522, 318, 595, 391],
 [749, 508, 692, 1245, 544, 522, 0, 852, 574, 1015],
 [539, 535, 694, 687, 482, 318, 852, 0, 489, 564],
 [691, 589, 694, 487, 419, 595, 574, 489, 0, 679],
 [569, 442, 714, 568, 574, 391, 1015, 564, 679, 0]]


# langs = ['1', '2','3','4','5','6','7','8','9','10']
# plt.bar(langs,sdv)
# plt.title('Social Distancing Violations (SDV) in a day')
# plt.xlabel('Device')
# plt.ylabel('NUmber of Violations')
# # plt.bar(langs,sdw)
# plt.show()





























# lat=[]
# lon=[]
# for i in final_data:
#     lat.append(i[0])
#     lon.append(i[1])
#
# df = DataFrame (lat,columns=['lat'])
# df['lon'] = lon
#
# df.to_csv('out.csv')
#
#
# geo.kgen([final_data],'h.kml',kml_type = 'line')




